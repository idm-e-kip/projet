# Planning du projet : 

| Nom |Prénom|Tâche|Temps estimé|Temps réel|
|--|--|--|--|--|
| 20/10/2021 |  |  |  |  |
| MICHOT | Julien | Création du métamodel | 1h | 1h30 |
| KOZLOV | Antoine | Création du métamodel | 1h | 1h30 |
| POCHET | Antoine | Création du métamodel | 1h | 1h30 |
| EL IDRISSI | Elyas | Création du métamodel | 1h | 1h30 |
| MICHOT | Julien |  Création du GIT et arborescence | 1h30 | 1h30 |
| KOZLOV | Antoine |  Création du GIT et arborescence | 1h30 | 1h30 |
| POCHET | Antoine | Relecture du métamodel et création du model | 1h30 | 1h30 |
| EL IDRISSI | Elyas | Relecture du métamodel et création du model | 1h30 | 1h30 |
| 27/10/2021 |  |  |  |  |
| TOUS |  | Résolution de problème technique et compréhension de SimpleMarkedown | 3h | 3h |
| MICHOT | Julien |  Début QVTO | 1h | 1h |
| KOZLOV | Antoine | Début Acceleo | 3h | 3h |
| POCHET | Antoine | Début QVTO | 1h | 1h |
| EL IDRISSI | Elyas | Début Acceleo | 2h | 2h |
| 10/11/2021 |  |  |  |  |
| TOUS | | QVTO | 1h30 | 3h30 |
| MICHOT | Julien | Remplissage du model | 1h | 1h |
| KOZLOV | Antoine | Acceleo | 1h | 1h |
| KOZLOV | Antoine | Remplissage du model | 1h | 2h |
| POCHET | Antoine | QVTO | 4h | 4h |
| EL IDRISSI | Elyas | Mise a jour du métamodel | 1h | 1h |
| 17/11/2021 |  |  |  |  |
| TOUS |  | Refactor du GIT | 1h | 1h |
| MICHOT | Julien | Xtext | 3h | 3h |
| KOZLOV | Antoine | Xtext | 3h | 3h |
| POCHET | Antoine | Xtext | 3h | 3h |
| EL IDRISSI | Elyas | Xtext | 3h | 3h |
| 24/11/2021 |  |  |  |  |
| MICHOT | Julien | Xtext | 3h | 3h |
| KOZLOV | Antoine | MM Bootstrap | 3h | 3h |
| POCHET | Antoine | Xtext | 3h | 3h |
| EL IDRISSI | Elyas | MM Bootstrap | 3h | 3h |
| 01/12/2021 |  |  |  |  |
| MICHOT | Julien | Refactor | 3h | 3h |
| KOZLOV | Antoine | MM Bootstrap | 3h | 3h |
| POCHET | Antoine | MM Intermédiraire Bootstrap | 3h | 3h |
| EL IDRISSI | Elyas | Refactor | 3h | 3h |
| 08/12/2021 |  |  |  |  |
| MICHOT | Julien | QVTO | 3h | 3h |
| KOZLOV | Antoine | MM Intermédiraire Bootstrap | 3h | 3h |
| POCHET | Antoine | QVTO | 3h | 3h |
| EL IDRISSI | Elyas | MM Intermédiraire Bootstrap | 3h | 3h |
| 15/12/2021 |  |  |  |  |
| MICHOT | Julien | QVTO Bootstrap | 3h | 3h |
| KOZLOV | Antoine | Acceleo Bootstrap | 3h | 3h |
| POCHET | Antoine | QVTO Bootstrap | 3h | 3h |
| EL IDRISSI | Elyas | Acceleo Bootstrap | 3h | 3h |

