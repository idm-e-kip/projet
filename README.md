# Projet : FIL

### Équipe :  e-kip

-   MICHOT Julien
-   KOZLOV Antoine
-   EL IDRISSI Elyas
-   POCHET Antoine

### Date :  17/11/2021

### Version :  2.0

## Fichiers

* ### **[Visualisation métamodèle](./Image/métamodèle.png)**

* ### **[Projets Eclipse métamodèle](./Eclipse/Métamodèle)**

* ### **[Projet Eclipse modèle](./Eclipse/Modèle)**

* ### **[Projet Eclipse Acceleo](./Eclipse/Acceleo)**

* ### **[Projet Eclipse QVTO](./Eclipse/TransformationQVTO)**

* ### **[Glossaire métier](./Glossaire/Glossaire.md)**