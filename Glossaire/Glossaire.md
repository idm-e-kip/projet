# Glossaire métier

* **Fil** : 
	
	Le fil est un portail pédagogique où l'on retrouve l'organisation et le contenu des UEs.

* **Formation** :
	
	La formation définie les types d'enseignements suivis.
	
* **Parcours** :
	
	Un parcours spécifie la formation.

* **Semestre** :

	Un semestre est une subdivision d'un parcours d'une formation. Une année est constituée de deux semestres.

* **UE** :

	Une UE est une matière.

* **Description** :

	Une description correspond à la presentation d'une formation, d'un parcours, ou une d'UE.

* **Ressource** :

	Une ressource est un fichier téléchargable.

* **Semainier** :

	Un semainier représente l'emploi du temps pour une formation, un parcours, ou une UE.

* **Personne** :

	Une personne peut être responsable de formations, de parcours, d'UEs et/où intervenant dans des UEs.

### [Retour](../README.md)
