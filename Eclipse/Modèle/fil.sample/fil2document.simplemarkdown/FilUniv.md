# FilUniv
# FilUniv
FIl de l'université de Lille

**Responsable : **

* Aubert Fabrice



# Master Informatique

Le master mention Informatique de l'Université de Lille propose une formation de pointe pour les étudiants qui ciblent un poste de cadre dans le secteur des sciences de l'information et des technologies des communications. Ce master constitue une poursuite d'études naturelle pour les étudiants titulaires d'une Licence Informatique généraliste. Cette mention propose notamment 6 parcours répartis sur 2 années qui permettent aux étudiants d'approfondir leurs compétences dans des domaines d'actualités. À l'issue d'une période de stage de fin d'études, les étudiants diplômés dans chacun des parcours peuvent aussi bien intégrer le monde professionnel sur des postes d'ingénieur ou poursuivre en doctorat pour s'ouvrir à la R&D et développer une expertise internationale.

**Responsable : **

* Rouvoy Romain



## Cloud Computing

Le parcours Cloud Computing forme des cadres en informatique qui se destinent aux métiers de la gestion d'infrastructures au sein des grands centres de données et de calculs. Il s'agit à la fois de former des informaticiens sensibles aux problématiques de sécurité informatique, d'organisation des systèmes et de gestion des architectures logicielles réparties, tout en couvrant l'évolution vers les technologies de virtualisation misent en œuvre dans le cloud. L'objectif de ce parcours est donc d'enseigner aux étudiants la conception et le déploiement de logiciels pour lesquels performances et sécurité sont prépondérantes. afin que les étudiants soient en mesure de tirer le meilleurs parti des infrastructures du cloud, mais aussi d'en assurer le bon fonctionnement et la maintenance, dans une logique de projet DevOps.


En lien étroit avec les grands acteurs du cloud computing (très présents dans notre région), et notamment avec leurs services de R&D, et leurs RSSI, mais sans négliger les sociétés de cybersécurité, le master cloud computing offre aux étudiants qui le suivent des connaissances et des compétences de pointes dans le domaine de la sécurité informatique, de la conception et du déploiement de logiciels dans le cloud, ainsi que de la gestion et de la maintenance des grands centre de calculs et de données. Le parcours insiste aussi sur les compétences en veille technologiques qui sont essentielles aux spécialistes de la sécurité. Les compétences acquises dans le parcours Cloud Computing sont les suivantes : maîtriser les concepts et les algorithmes de la répartition et de la distribution ; maitriser les outils et les problématiques de la sécurité informatique : cryptographie, pen testing, systèmes de détection d'intrusions... ; à travers l'apprentissage des pratiques devops, être en mesure d'intervenir dans toutes les étapes du de la conception et du déploiement des grands logiciels ; comprendre et maitriser les liens entre l'usage des infrastructures et les performances des logiciels qui y sont déployés ; savoir constituer rapidement un état de l'art (veille technologique) pour s'adapter aux incessantes évolutions technologiques ; être capable d'intégrer une équipe devops; connaître les grands principes de l'entrepreneuriat en informatique côté startup ; savoir intégrer une équipe, s'exprimer dans un cadre professionnel...


La région regorge d'entreprises, allant de la startup à la multi-nationnale, consacré au Cloud Computing, et/ou à la cyber-sécurité. Les diplômés du master (anciennement appelé TIIR), trouvent principalement des débouchés dans les entreprises de Cloud Computing, dans les sociétés d'expertise en sécurité informatique, au sein des services de R&D ou au titre de RSSI dans des grands groupes, des PMEs, ou encore dans des laboratoires publiques. Ils occupent pour le plus souvent des emplois de cadre (BAC+5) en CDI, qui sont en moyenne trouvés dans les 15 jours qui suivent la fin de leurs études. Chaque année une fraction des étudiants poursuit ses études en troisième cycle universitaire, c'est-à-dire en Thèse de Doctorat en Informatique.

**Responsable : **

* Grimaud Gilles



### S1
#### ACT
UE obligatoire


Le but de l'algorithmique peut être résumé en " trouver un "bon" algorithme " pour un problème donné. Cela soulève de namebreuses questions...


ressourceACT : www.google.com/act


semainierACT

**Responsable : **

* Derbel Dilel



* Tison Sophie



* Voge Marie-Emilie



**Intervenant : **

* Berveglieri N



* Bilasco M



* Derbel Dilel



* Fortin P



* Tison Sophie



* Voge Marie-Emilie



#### IIR
UE obligatoire


INITIATION À L'INNOVATION ET À LA RECHERCHE


ressourcesIIR : www.google.com/iir


ressourcesIIR2 : www.google.com/ressourcesIIR2


semainierIIR

**Responsable : **

* Le Pallec Xavier



**Intervenant : **

* Belmonte Romain



* Duchien Laurence



* Le Pallec Xavier



* Plénacoste Patricia



* Tarby Jean-Claude



#### DAC
UE obligatoire


L’unité “Deploiement et Administration du Cloud” vise à acquérir les fondamentaux liés à l’administration du Cloud, à la gestion des infrastructures et au déploiement d’applications scalables : configurer un serveur ou un ensemble de serveurs dans le cloud, mettre en place une pile logicielle efficace, deployer des applications, organiser le suivi (monitoring) des infrastructures et des systèmes administrés.


Configuration de systèmes unix, utilisation d’openstack, mise en place de docker, apprentissage d’Ansible et mise en place d’un loadblancer.


Savoir lire de la documentation en anglais, savoir apprendre des ses erreurs, être en mesure de rédigier des rapports technique bref et savoir communiquer.

**Responsable : **

**Intervenant : **

### S2
#### ISI
UE obligatoire


Les objectifs de ce module sont les suivants : Connaitre les concepts principaux de sécurité (authentification, confidentalité, etc.) et les risques associés, connaitre les outils permettant de détecter les vulnérabilités logicielles (fuzzing, analyse statique, etc.) et savoir déployer des stratégies de mitigation pour limiter l’impact d’une vulnérabilité (séparation de privilèges, virtualisation, etc.)

**Responsable : **

* Grimaud Gilles



**Intervenant : **

#### COA
UE obligatoire


L’objectif de ce module est de apprendre la programmation avancée avec le langage de programmation C/C++. Le langage C/C++ est encore largement utilisé, surtout dans la programmation des système embarqués. En effet, le langage C/C++ permet la programmation à bas niveau pour intéragir directement avec le matériel, et un contrôle très precis de l’utilisation de mémoire. En générale, le compilateur C++ est capagle de générer du code très optimisé et reduit en terme d’utilisation de mémoire et d’énergie. Les efforts de standardization récentes (C++11, C++14 et C++17) ont ajouté une grande namebre de fonctionnalité comme les lambda functions, l’induction sur les types, la meta-programmation, etc. qui ont modernisé le langage.


Les bases du langage. Le modèle de mémoire. Le pointeurs intelligents (smart pointers). La programmation fonctionnel en C++. Les templates. Éléments de meta-programmation.


Connaissance approfondie du langage C/C++, techniques de programmation avec de pointeurs intélligents et techniques Metaprogrammation

**Responsable : **

* Giuseppe Lipari



**Intervenant : **

* Caron Anne-Cécile



* Dumoulin Cedric



* Belmonte Romain



* Duchien Laurence



* Giuseppe Lipari



* Launay Michael



* Le Pallec Xavier



* Plénacoste Patricia



* Salvati Sylvain



* Tarby Jean-Claude



#### Compil
UE obligatoire


Connaître l’organisation usuelle d’un compilateur et les différentes phases de la compilation (arbre de syntaxe abstraite, représentations intermédiaires, assembleur, code machine). Savoir écrire un compilateur pour un langage simple.


Présentation de l’organisation générale d’un compilateur. Écriture progressive d’un compilateur pour un petit langage : interprète, génération de code C, génération d’assembleur. Présentation d’un fragment suffisant de l’assembleur x86 pour compiler un petit langage et, de façon plus générale, du modèle d’exécution de la machine


Maîtriser les représentations intermédiaires dans un processus de compilation. Savoir écrire un compilateur pour un langage simple.

**Responsable : **

* Hym Samuel



**Intervenant : **

* Dumoulin Cedric



* Fortin P



* Giuseppe Lipari



* Grimaud Gilles



#### FOC
UE obligatoire


L’unité “Fondement de la cryptographie” enseigne les modèles de sécurité cryptographique fondamentaux. L’étudiant y apprend le fonctionnement de la crpytographie symétrique puis de la cryptographie asymétrique. Il acquière une culture générale en la matière suffisante pour pouvoir discuter de la sécurité induite par l’utilisation de ces outils.


Principe de la cryptographie symétrique, asymétrique et du hashage crpytographique.

**Responsable : **

* Sedoglavic Alexandre



**Intervenant : **

#### RLO
UE obligatoire


Approfondir certaines notions de réseau informatique abordées en Licence 3. Maîtriser les concepts, les protocoles et les technologies utilisés sur les réseaux locaux modernes. Acquérir les connaissances et savoir-faire nécessaires pour comprendre et concevoir les architectures des réseaux locaux des entreprises et des centres de données


Être capable de concevoir une architecture réseau adaptée en prenant en compte les contraintes de performance et de sécurité. Être en mesure de comprendre le fonctionnement d’une infrastructure réseau existante afin de discerner les faiblesses, d’en améliorer les caractéristiques et de solutionner les dysfonctionnements

**Responsable : **

* Buche Xavier



**Intervenant : **

* Duchien Laurence



* Dumoulin Cedric



* Fortin P



* Giuseppe Lipari



#### SDS
UE optionnelle


Approfondir les notions abordées en M1 sur la recherche de vulnerabilités dans des programmes. Comprendre comment les vulnérabilités sont exploitées, quelle contre-mesures existent, et les limitations de ce celles-ci. Savoir limiter l’impact de l’exploitation de vulnérabilités en isolant les services concernés


Connaitre les principaux type de vulnérabilités, leur mode d’exploitation, et les différentes solutions à mettre en oeuvre face à celles-ci.

**Responsable : **

* Ballabriga Clément



**Intervenant : **

## e-Services

Le parcours E-Services s’articule autour de la conception et du développement des services numériques, c’est-à-dire des services accessibles au travers des moyens de communication numérique - notamment l’Internet – mais également au travers de tous les équipements liés à l’éconameie digitale : smartphones, tablettes, interfaces multi-touch, interfaces vocales, etc. Le parcours E-Services repose sur le développement de projets complets (de la conception à la réalisation et la promotion), en se concentrant sur les aspect utilisateurs, usages, IHM, UX, ainsi que des travaux autour de l'innovation et du rapport à la technologie.


En étroite liaison avec les mondes de l’entreprise, de la recherche et de l’innovation, le parcours E-Services a pour objectif de donner des connaissances et des compétences opérationnelles en matière de conception de services numériques, d’interfaces homme-machine, de gestion de projets, et de design applicatif. Il contient un ensemble cohérent de matières et de technologies à apprendre, à intégrer, mais aussi à expérimenter et à découvrir, pour être capable d’analyser le besoin des clients et proposer des solutions numériques innovantes, tout en étudiant leurs usages potentiels pour proposer la meilleure solution, principalement autour de l’expérience utilisateur (UX). Les enseignements de ce parcours mettent l’accent sur l’ergonameie et la conception graphique, la créativité, le génie logiciel, la gestion de projets, les infrastructures front et back (Java EE, .NET, etc.), et les langages et technologies pour le Web et pour les applications connectées (JavaScript, HTML5, Android, frameworks web, etc.).


Nos diplômés travaillent dans des entreprises de services du numérique (ESN), de transformation numérique, de technologie et d'ingénierie, de services d'infogérance, des PME/TPE ou des start-ups. Les métiers sont variés : maîtrise d'ouvrage, conception de produit, développement de solutions applicatives web et mobiles, marketing digital… et dans des domaines tels que santé, énergie, retail, transports, industrie, aérospatiale, media, banque, assurance, etc. Nos diplômés occupent le plus souvent des emplois de cadre (Bac+5) en CDI, dans les 15 jours qui suivent la fin de leurs études (+99% d’insertion). La poursuite en Thèse de Doctorat en Informatique est possible à l'issue de ce parcours.

**Responsable : **

* Grimaud Gilles



### S1
#### ACT
UE obligatoire


Le but de l'algorithmique peut être résumé en " trouver un "bon" algorithme " pour un problème donné. Cela soulève de namebreuses questions...


ressourceACT : www.google.com/act


semainierACT

**Responsable : **

* Derbel Dilel



* Tison Sophie



* Voge Marie-Emilie



**Intervenant : **

* Berveglieri N



* Bilasco M



* Derbel Dilel



* Fortin P



* Tison Sophie



* Voge Marie-Emilie



#### IIR
UE obligatoire


INITIATION À L'INNOVATION ET À LA RECHERCHE


ressourcesIIR : www.google.com/iir


ressourcesIIR2 : www.google.com/ressourcesIIR2


semainierIIR

**Responsable : **

* Le Pallec Xavier



**Intervenant : **

* Belmonte Romain



* Duchien Laurence



* Le Pallec Xavier



* Plénacoste Patricia



* Tarby Jean-Claude



#### TAC
UE obligatoire


Technologies pour Application Connectées


ressourcesTAC : www.google.com/tac


semainierTAC

**Responsable : **

* Tarby Jean-Claude



**Intervenant : **

* Tarby Jean-Claude



### S2
#### BDR
UE obligatoire


Bases de données relationnelles


ressourcesBDR : www.google.com/bdr


semainierBDR

**Responsable : **

* Caron Anne-Cécile



* Salvati Sylvain



**Intervenant : **

* Caron Anne-Cécile



* Salvati Sylvain



#### ECLE
UE obligatoire


Ergonameie et Conception des Logiciels et E-services


ressourcesECLE : www.google.com/ecle


semainierECLE

**Responsable : **

* Tarby Jean-Claude



**Intervenant : **

* Tarby Jean-Claude



* Belmonte Romain



* Caron Anne-Cécile



* Duchien Laurence



* Dumoulin Cedric



* Giuseppe Lipari



* Launay Michael



* Le Pallec Xavier



* Plénacoste Patricia



* Salvati Sylvain



#### ISI
UE obligatoire


Les objectifs de ce module sont les suivants : Connaitre les concepts principaux de sécurité (authentification, confidentalité, etc.) et les risques associés, connaitre les outils permettant de détecter les vulnérabilités logicielles (fuzzing, analyse statique, etc.) et savoir déployer des stratégies de mitigation pour limiter l’impact d’une vulnérabilité (séparation de privilèges, virtualisation, etc.)

**Responsable : **

* Grimaud Gilles



**Intervenant : **

#### SR
UE obligatoire


Cette UE vise à présenter les fondamentaux des systèmes répartis et les principes qui guident toute interaction client/serveur.


Rappel Modèle OSI, focus couche applicative, notion de protocoles (wrt. couche). Passage à l’échelle et gestion de la concurrence. Synchronisation et sérialisation des données. Intégration de systèmes et interopérabilité. Architectures orientées service et RPC. Modèles de communications asynchrones

**Responsable : **

* Rouvoy Romain



**Intervenant : **

#### IIHM
UE optionnelle


L'UE vise à faire prendre conscience aux étudiants l'importance de considérer les besoins et les caractéristiques des utilisateurs finaux dans le développement de systèmes interactifs. Le contenu se concentre sur la démarche de conception centrée utilisateurs et le maquettage. Tous les aspects liés aux développement d'interfaces graphiques interactives sont également étudiés..

**Responsable : **

* Pietrzak Thomas



**Intervenant : **

* Malacria Sylvain



## Génie Logiciel

Le parcours Génie Logiciel s’articule autour de la conception et du développement des logiciels agiles, c'est-à-dire des applications dont les fonctionnalités sont en parfaite adéquation avec les besoins des utilisateurs. Le parcours Génie Logiciel allie donc méthodes, concepts et technologies pour favoriser l’émergence d’applications innovantes sans jamais perdre de vue l’efficacité. Les domaines applicatifs étudiés s’étendent depuis l’informatique embarquée jusqu’aux grands systèmes répartis.


En lien étroit avec le monde de l’entreprise, de la recherche et de l’innovation, les étudiants suivant le parcours Génie Logiciel acquièrent des connaissances et des compétences applicables au développement et à la maintenance des systèmes d’informations modernes. De plus, la sensibilisation à la veille technologique favorise la transition vers l’intégration et le déploiement effectif de nouvelles technologies logicielles. Les compétences acquises dans le parcours Génie Logiciel sont les suivantes : maîtriser les concepts et les outils du développement logiciel; être capable d’intervenir dans toutes les étapes du cycle de vie des applications; savoir adopter un processus de développement garantissant la production d’un logiciel performant et de qualité; savoir gérer efficacement une équipe de développement; maîtriser les technologies phares tout en sachant s’adapter à celles qui les remplaceront; être capable d’intégrer une équipe agile; connaître les grands principes de l’entrepreneuriat en informatique côté startup; savoir mettre en place une veille technologique.


ressourcesParcoursGL : www.google.com/parcours/gl


Le parcours Génie Logiciel forme des informaticiens de niveau bac+5 spécialisés dans la conception, la mise en œuvre, le déploiement et la maintenance d’applications innovantes fortement connectées aux besoins des utilisateurs. Nos diplômés travaillent dans des ESN (Entreprises de services du numérique), des DSI (Direction des systèmes d’information) de grands groupes, des PME, des laboratoires de recherche publics et privés, où ils occupent un emploi en CDI. Avec 99% d’insertion pour un délai de recherche d’emploi de 15 jours en moyenne, on peut parler de situation idéale. La poursuite d'études en thèse de doctorat en informatique est possible à l'issue de ce parcours.

**Responsable : **

* Quinton Clément



### S1
#### IIR
UE obligatoire


INITIATION À L'INNOVATION ET À LA RECHERCHE


ressourcesIIR : www.google.com/iir


ressourcesIIR2 : www.google.com/ressourcesIIR2


semainierIIR

**Responsable : **

* Le Pallec Xavier



**Intervenant : **

* Belmonte Romain



* Duchien Laurence



* Le Pallec Xavier



* Plénacoste Patricia



* Tarby Jean-Claude



#### IDM
UE obligatoire


Ingénierie des Modèles


ressourcesIDM : www.google.com/idm


semainierIDM

**Responsable : **

* Dumoulin Cedric



**Intervenant : **

* Dumoulin Cedric



* Launay Michael



* Le Pallec Xavier



#### ACT
UE obligatoire


Le but de l'algorithmique peut être résumé en " trouver un "bon" algorithme " pour un problème donné. Cela soulève de namebreuses questions...


ressourceACT : www.google.com/act


semainierACT

**Responsable : **

* Derbel Dilel



* Tison Sophie



* Voge Marie-Emilie



**Intervenant : **

* Berveglieri N



* Bilasco M



* Derbel Dilel



* Fortin P



* Tison Sophie



* Voge Marie-Emilie



### S2
#### BDR
UE obligatoire


Bases de données relationnelles


ressourcesBDR : www.google.com/bdr


semainierBDR

**Responsable : **

* Caron Anne-Cécile



* Salvati Sylvain



**Intervenant : **

* Caron Anne-Cécile



* Salvati Sylvain



#### COA
UE obligatoire


L’objectif de ce module est de apprendre la programmation avancée avec le langage de programmation C/C++. Le langage C/C++ est encore largement utilisé, surtout dans la programmation des système embarqués. En effet, le langage C/C++ permet la programmation à bas niveau pour intéragir directement avec le matériel, et un contrôle très precis de l’utilisation de mémoire. En générale, le compilateur C++ est capagle de générer du code très optimisé et reduit en terme d’utilisation de mémoire et d’énergie. Les efforts de standardization récentes (C++11, C++14 et C++17) ont ajouté une grande namebre de fonctionnalité comme les lambda functions, l’induction sur les types, la meta-programmation, etc. qui ont modernisé le langage.


Les bases du langage. Le modèle de mémoire. Le pointeurs intelligents (smart pointers). La programmation fonctionnel en C++. Les templates. Éléments de meta-programmation.


Connaissance approfondie du langage C/C++, techniques de programmation avec de pointeurs intélligents et techniques Metaprogrammation

**Responsable : **

* Giuseppe Lipari



**Intervenant : **

* Caron Anne-Cécile



* Dumoulin Cedric



* Belmonte Romain



* Duchien Laurence



* Giuseppe Lipari



* Launay Michael



* Le Pallec Xavier



* Plénacoste Patricia



* Salvati Sylvain



* Tarby Jean-Claude



#### ISI
UE obligatoire


Les objectifs de ce module sont les suivants : Connaitre les concepts principaux de sécurité (authentification, confidentalité, etc.) et les risques associés, connaitre les outils permettant de détecter les vulnérabilités logicielles (fuzzing, analyse statique, etc.) et savoir déployer des stratégies de mitigation pour limiter l’impact d’une vulnérabilité (séparation de privilèges, virtualisation, etc.)

**Responsable : **

* Grimaud Gilles



**Intervenant : **

#### Compil
UE obligatoire


Connaître l’organisation usuelle d’un compilateur et les différentes phases de la compilation (arbre de syntaxe abstraite, représentations intermédiaires, assembleur, code machine). Savoir écrire un compilateur pour un langage simple.


Présentation de l’organisation générale d’un compilateur. Écriture progressive d’un compilateur pour un petit langage : interprète, génération de code C, génération d’assembleur. Présentation d’un fragment suffisant de l’assembleur x86 pour compiler un petit langage et, de façon plus générale, du modèle d’exécution de la machine


Maîtriser les représentations intermédiaires dans un processus de compilation. Savoir écrire un compilateur pour un langage simple.

**Responsable : **

* Hym Samuel



**Intervenant : **

* Dumoulin Cedric



* Fortin P



* Giuseppe Lipari



* Grimaud Gilles



#### SR
UE obligatoire


Cette UE vise à présenter les fondamentaux des systèmes répartis et les principes qui guident toute interaction client/serveur.


Rappel Modèle OSI, focus couche applicative, notion de protocoles (wrt. couche). Passage à l’échelle et gestion de la concurrence. Synchronisation et sérialisation des données. Intégration de systèmes et interopérabilité. Architectures orientées service et RPC. Modèles de communications asynchrones

**Responsable : **

* Rouvoy Romain



**Intervenant : **

## Internet des Objets

Les objets de notre vie quotidienne sont désormais équipés de capteurs et de processeurs, et souvent connectés en réseaux : dans les bâtiments pour réduire leur consommation énergétique, dans les voitures pour aider à la conduite, dans les dispositifs médicaux, etc. On parle de l’Internet des Objets (Internet of Things). La diffusion de l’électronique embarquée pose des nouveaux enjeux dans la conception, la réalisation et la gestion de ces dispositifs. Il faut d’abord garantir la sécurité pour ne pas les transformer en instruments de surveillance de masse. Il faut une attention spécifique à l’utilisation des ressources et à la consommation énergétique pour réduire leur empreinte écologique. Certains décisions prises par un algorithme peuvent mettre en danger la vie humaine, donc il faut assurer la correction de ces systèmes. L'objectif de ce parcours du Master Informatique est de former des professionnels capables de concevoir et programmer des systèmes embarqués de manière à réduire leur coût, tout en garantissant leur sécurité et leur fiabilité. Le parcours IoT vise à former un profil interdisciplinaire, car les diplômés collaboreront avec des experts d'autre domaines (électroniciens, mécaniciens, médecins, etc.).


Les diplômés du parcours Internet des objets seront capables de concevoir et développer un système à moindre coût en terme de ressources et à bas consommation énergétique. Ils seront expert de systèmes d'exploitation, de réseaux de capteurs, de protocoles de communication sans fils, des systèmes en temps réel. Ils seront aussi préparés pour analyser un système avec l'objectif de détecter de failles potentielles de sécurité. Pour renforcer les aspects interdisciplinaires, la formation est complétée par des notions de base de communication radio sans fils et des d’automatique. Les notions théoriques sont renforcées avec des projets pratiques de programmation des systèmes embarqués, de robotique et de réseaux de capteurs. L’initiation à la recherche trouve une place essentielle dans le parcours IoT dans un domaine en évolution permanente avec de fortes perspectives d’expansion.


Les diplômés du parcours IoT auront l’opportunité de travailler dans le domaine de l'informatique des systèmes embarqués, un secteur en forte expansion. Il s'agit de sociétés de service qui opèrent dans les domaines de l'avionique, le ferroviaire, la robotique industrielle ; il s'agit également de sociétés qui travaillent dans le secteur des produits électroniques grand public, pour augmenter les objets de la vie quotidienne (électroménagers, systèmes de chauffage, etc.) avec de l'intelligence. Il s'agit aussi d'entreprises start-up qui conçoivent et produisent des produits innovatifs. Cette formation fait partie du parcours Graduate School de l'Université de Lille. La poursuite d'études en Thèse de Doctorat est possible à l'issue de cette formation.

**Responsable : **

* Giuseppe Lipari



### S1
#### ACT
UE obligatoire


Le but de l'algorithmique peut être résumé en " trouver un "bon" algorithme " pour un problème donné. Cela soulève de namebreuses questions...


ressourceACT : www.google.com/act


semainierACT

**Responsable : **

* Derbel Dilel



* Tison Sophie



* Voge Marie-Emilie



**Intervenant : **

* Berveglieri N



* Bilasco M



* Derbel Dilel



* Fortin P



* Tison Sophie



* Voge Marie-Emilie



#### IIR
UE obligatoire


INITIATION À L'INNOVATION ET À LA RECHERCHE


ressourcesIIR : www.google.com/iir


ressourcesIIR2 : www.google.com/ressourcesIIR2


semainierIIR

**Responsable : **

* Le Pallec Xavier



**Intervenant : **

* Belmonte Romain



* Duchien Laurence



* Le Pallec Xavier



* Plénacoste Patricia



* Tarby Jean-Claude



#### CLE
UE obligatoire


L’objectif de cette UE est de former les étudiants aux méthodologies de programmation spécifiques aux systèmes embarqués.


Understand the main problems and constraints in the design and programming of an embedded system. Know the basis of feedback control to communicate with control engineers. Code generation from high level models of finite state machine. Implementation of a simple embedded system on a real robotic platform.

**Responsable : **

* Giuseppe Lipari



**Intervenant : **

### S2
#### ISI
UE obligatoire


Les objectifs de ce module sont les suivants : Connaitre les concepts principaux de sécurité (authentification, confidentalité, etc.) et les risques associés, connaitre les outils permettant de détecter les vulnérabilités logicielles (fuzzing, analyse statique, etc.) et savoir déployer des stratégies de mitigation pour limiter l’impact d’une vulnérabilité (séparation de privilèges, virtualisation, etc.)

**Responsable : **

* Grimaud Gilles



**Intervenant : **

#### COA
UE obligatoire


L’objectif de ce module est de apprendre la programmation avancée avec le langage de programmation C/C++. Le langage C/C++ est encore largement utilisé, surtout dans la programmation des système embarqués. En effet, le langage C/C++ permet la programmation à bas niveau pour intéragir directement avec le matériel, et un contrôle très precis de l’utilisation de mémoire. En générale, le compilateur C++ est capagle de générer du code très optimisé et reduit en terme d’utilisation de mémoire et d’énergie. Les efforts de standardization récentes (C++11, C++14 et C++17) ont ajouté une grande namebre de fonctionnalité comme les lambda functions, l’induction sur les types, la meta-programmation, etc. qui ont modernisé le langage.


Les bases du langage. Le modèle de mémoire. Le pointeurs intelligents (smart pointers). La programmation fonctionnel en C++. Les templates. Éléments de meta-programmation.


Connaissance approfondie du langage C/C++, techniques de programmation avec de pointeurs intélligents et techniques Metaprogrammation

**Responsable : **

* Giuseppe Lipari



**Intervenant : **

* Caron Anne-Cécile



* Dumoulin Cedric



* Belmonte Romain



* Duchien Laurence



* Giuseppe Lipari



* Launay Michael



* Le Pallec Xavier



* Plénacoste Patricia



* Salvati Sylvain



* Tarby Jean-Claude



#### Compil
UE obligatoire


Connaître l’organisation usuelle d’un compilateur et les différentes phases de la compilation (arbre de syntaxe abstraite, représentations intermédiaires, assembleur, code machine). Savoir écrire un compilateur pour un langage simple.


Présentation de l’organisation générale d’un compilateur. Écriture progressive d’un compilateur pour un petit langage : interprète, génération de code C, génération d’assembleur. Présentation d’un fragment suffisant de l’assembleur x86 pour compiler un petit langage et, de façon plus générale, du modèle d’exécution de la machine


Maîtriser les représentations intermédiaires dans un processus de compilation. Savoir écrire un compilateur pour un langage simple.

**Responsable : **

* Hym Samuel



**Intervenant : **

* Dumoulin Cedric



* Fortin P



* Giuseppe Lipari



* Grimaud Gilles



#### SR
UE obligatoire


Cette UE vise à présenter les fondamentaux des systèmes répartis et les principes qui guident toute interaction client/serveur.


Rappel Modèle OSI, focus couche applicative, notion de protocoles (wrt. couche). Passage à l’échelle et gestion de la concurrence. Synchronisation et sérialisation des données. Intégration de systèmes et interopérabilité. Architectures orientées service et RPC. Modèles de communications asynchrones

**Responsable : **

* Rouvoy Romain



**Intervenant : **

#### SDS
UE optionnelle


Approfondir les notions abordées en M1 sur la recherche de vulnerabilités dans des programmes. Comprendre comment les vulnérabilités sont exploitées, quelle contre-mesures existent, et les limitations de ce celles-ci. Savoir limiter l’impact de l’exploitation de vulnérabilités en isolant les services concernés


Connaitre les principaux type de vulnérabilités, leur mode d’exploitation, et les différentes solutions à mettre en oeuvre face à celles-ci.

**Responsable : **

* Ballabriga Clément



**Intervenant : **

#### HD
UE obligatoire


Apprentissage des méthodes et outils de conception VHDL et d’implantation FPGA. Les architectures embarquées sont conçues en utilisant des langages spécifiques comme le VHDL ainsi que des outils dédiés tel que Vivado de Xilinx. Ces langages et environnements de conception permettent l’usage systématique de concepts liés au matériel (parallélisme, horloge, modules hiérarchiques, ..etc). L’objectif de ce cours est d’introduire les concepts de programmation de matériel et les mettre en pratique au travers du VHDL et Vivado pour la réalisation d’architectures simples mais en utilisant les mêmes outils et approches que les industries modernes.


Architecture élémentaire, logique combinatoire, machine à états finie,

**Responsable : **

* Dekeyser Jean-Luc



**Intervenant : **

## Machine Learning

L'apprentissage machine est désormais au cœur des techniques d'intelligence artificielle qui envahissent notre quotidien. Cette évolution transforme profondément les systèmes d'information classiques et pose de nouveaux enjeux dans la gestion et l'analyse des données. Comprendre comment collecter, organiser les données et passer à l'échelle de très grands volumes, mais surtout comprendre, comparer et mesurer les nouveaux algorithmes d'apprentissage machine font partie de ces enjeux. Ce sont les objectifs du parcours Machine learning, qui a la particularité de développer à la fois des connaissances scientifiques fondamentales poussées et des compétences techniques avancées, qualités essentielles pour appréhender les évolutions rapides d'un domaine en cours de maturation. Destiné aux métiers de la sciences de données, du big data, de l'apprentissage machine et de l'intelligence artificielle, le parcours Machine learning est une formation solide en informatique. Le programme proposé traduit la volonté assumée de privilégier les compétences de développement fortement assises sur des connaissances théoriques solides en apprentissage automatique, algorithmique et bases de données.


Les compétences acquises lors de ce parcours Machine Learning sont principalement liées aux techniques d'intelligence artificielle pour l'exploitation, l'analyse et la prévision à partir de grandes masses de données.Les enseignements se concentrent sur trois piliers : l'apprentissage machine, l'algorithmique et les bases de données. A l'issue du master, les étudiants participeront à la conception et la mise en œuvre de solutions basées sur l'apprentissage machine supervisé, non supervisé et par renforcement ; les bases de données avancées au delà du modèle relationnel et du SQL ; les méthodes d'optimisation au cœur de l'apprentissage ; les techniques de fouille de données, d'apprentissage profond, à partir de données textuelles ou de données en réseau. La formation est complétée par des compétences permettant d'intégrer une équipe et connaître les grands principes de l'entrepreneuriat en informatique. La place de l'initiation à la recherche dans le master est essentielle dans ce domaine en révolution permanente, pour mettre en place une veille technologique et maîtriser les technologies phares tout en sachant s'adapter à celles qui les remplaceront.


Les diplômés du master auront l'opportunité de travailler dans les secteurs en forte expansion utilisant les techniques d'intelligence artificielle et d'apprentissage machine. Il s'agit des sociétés de service en informatique, dans les départements dédiés à la décision, la prévision, ou encore dans les services de recherche et développement de domaines variés comme : la distribution, la gestion des ventes, la stratégie commerciale, la santé, les télécommunications, la géolocalisation, les technologies du web, le développement de progiciels, la gestion de l'énergie, les transports, la banque, l'assurance... La poursuite d'études en thèse de doctorat en informatique est possible à l'issue de ce parcours.

**Responsable : **

* Tommasi Marc



### S1
#### ACT
UE obligatoire


Le but de l'algorithmique peut être résumé en " trouver un "bon" algorithme " pour un problème donné. Cela soulève de namebreuses questions...


ressourceACT : www.google.com/act


semainierACT

**Responsable : **

* Derbel Dilel



* Tison Sophie



* Voge Marie-Emilie



**Intervenant : **

* Berveglieri N



* Bilasco M



* Derbel Dilel



* Fortin P



* Tison Sophie



* Voge Marie-Emilie



#### IIR
UE obligatoire


INITIATION À L'INNOVATION ET À LA RECHERCHE


ressourcesIIR : www.google.com/iir


ressourcesIIR2 : www.google.com/ressourcesIIR2


semainierIIR

**Responsable : **

* Le Pallec Xavier



**Intervenant : **

* Belmonte Romain



* Duchien Laurence



* Le Pallec Xavier



* Plénacoste Patricia



* Tarby Jean-Claude



#### SD
UE obligatoire


L’objectif principal de cette UE est de former les étudiants aux méthodes modernes d’apprentissage qui sont aujourd’hui au coeur d’un grand namebre de systèmes utilisés quotidiennement (les moteurs de recherche, les services de recommandation, les appareils photo détectant les visages, les téléphones portables apprenant des profils, les services vocaux reconnaissant la parole, .). Ainsi, les étudiants auront les compétences pour comprendre le monde numérique qui les entoure et l’utilisation qui peut être faite de données (données laissées sur le web, collectées par les opérateurs réseau ou de téléphonie etc.). Ils auront aussi les bases pour concevoir eux-mêmes des systèmes traitant automatiquement des données, pour constituer des bases de données pouvant être utilisées par un système d’apprentissage (comprenant l’importance d’une collecte et d’une annotation de qualité et la valeur de ces données). On s’attachera aussi à comprendre les qualités et les défauts de différents algorithmes afin d’associer le bon algorithme (et ses paramètres) à un problème donné. Un autre objectif sera de permettre aux étudiants de se forger un esprit critique quant aux résultats produits par les systèmes d’apprentissage (notion de risque, de validation croisée, de sur-apprentissage, de biais etc.). Toutes ces notions seront assimilées très pratiquement sur des cas concrets, en s’appuyant sur les bibliothèques logicielles les plus performantes du moment.


L’apprentissage automatique (Machine Learning en anglais) est un champ de l’intelligence artificielle au cur de la science des données. On distingue apprentissage supervisé, apprentissage non supervisé et apprentissage par renforcement. Ce cours abordera les deux premiers types d’apprentissage, le troisième étant traité dans les modules « prise de décision séquentielle dans l’incertain » et « apprentissage par renforcement » au S3. L’apprentissage automatique s’appuie sur des données : l’apprentissage non supervisé a pour objectif de regrouper les données selon leur similarité : des données ressemblantes sont regroupées ; une fois les données ainsi regroupées, l’apprentissage supervisé consiste à associer une nouvelle donnée au groupe dont elle est le plus proche.Le cours présentera les algorithmes les plus importants pour ces deux types d’apprentissage ainsi que leur mise en oeuvre : arbre de décision, approche bayésienne, séparateur à vaste marge, méthode d’ensemble (forêts aléatoires) pour l’apprentissage supervisé ; k-moyennes, k-médianes, segmentation hiérarchique et une approche bayésienne pour l’apprentissage non supervisé. D’autres algorithmes seront vus dans d’autres modules (notamment les réseaux de neurones dans l’UE « deep learning » au S2).En apprentissage supervisé, un élément capital consiste à évaluer les performances de l’algorithme pour permettre de répondre à la question : l’algorithme prédit telle chose : quelle est la probabilité que cette prdéction soit exacte ? Nous étudierons les méthodes permettant une telle évaluation, découpage stratifié du jeu d’exemples en ensemble d’entraînement et ensemble de test et validation croisée.On abordera également les techniques de pretraitement des données qui consistent à mettre les données sous une forme apte à leur traitement par un algorithme d’apprentissage.Toutes ces notions seront mises en pratique au fil du cours afin de lier théorie et application. Nous utiliserons pour cela des environnements logiciels de l’état de l’art.

**Responsable : **

**Intervenant : **

### S2
#### ISI
UE obligatoire


Les objectifs de ce module sont les suivants : Connaitre les concepts principaux de sécurité (authentification, confidentalité, etc.) et les risques associés, connaitre les outils permettant de détecter les vulnérabilités logicielles (fuzzing, analyse statique, etc.) et savoir déployer des stratégies de mitigation pour limiter l’impact d’une vulnérabilité (séparation de privilèges, virtualisation, etc.)

**Responsable : **

* Grimaud Gilles



**Intervenant : **

#### BDR
UE obligatoire


Bases de données relationnelles


ressourcesBDR : www.google.com/bdr


semainierBDR

**Responsable : **

* Caron Anne-Cécile



* Salvati Sylvain



**Intervenant : **

* Caron Anne-Cécile



* Salvati Sylvain



## Réalité Virtuelle et Augmenté

Le parcours "Réalité Virtuelle et Augmentée" permet d'acquérir une spécialisation dans le domaine de l'informatique graphique et de l'image. Ce parcours allie les grands thèmes de ce domaine informatique : la vision artificielle, la réalisation d'applications 3D, la conception, l'implémentation et l'évaluation de systèmes interactifs, ainsi que l'intelligence artificielle pour l'image. Au delà d'une formation solide dans chacun de ces thèmes, la réunion des compétences acquises permet d'aborder la conception et la réalisation d' applications en Réalité Virtuelle et en Réalité Augmentée.


Les compétences acquises dans le parcours Réalité Virtuelle et Augmentée sont : savoir réaliser une application 3D; savoir réaliser des systèmes de vision artificielle; connaitre le traitement et l'analyse de l'image et de la vidéo; connaitre les méthodes d'apprentissage sur l'image; savoir concevoir l'Interaction Humain-Machine et savoir l'évaluer; allier les thèmes de l'image, de l'interaction et de la 3D pour la conception d'applications de type Réalité Augmentée.


Le parcours Réalité Virtuelle et Augmentée forme des informaticiens de niveau bac+5. La spécialisation du parcours offre de plus l'opportunité de travailler dans la conception des applications informatiques liées à l'image : les environnements 3D virtuels interactifs (serious game pour la pédagogie ou la sécurité, valorisation de produits, visites virtuelles...), l'analyse de l'image et de la vidéo (satellite, caméra de sécurité, reconnaissance automatique...), la réalité augmentée (perception réelle augmentée, annotation virtuelle sur le réel...). La poursuite d'études en Thèse de Doctorat en Informatique est possible à l'issue de cette spécialité.

**Responsable : **

* Pietrzak Thomas



### S1
#### ACT
UE obligatoire


Le but de l'algorithmique peut être résumé en " trouver un "bon" algorithme " pour un problème donné. Cela soulève de namebreuses questions...


ressourceACT : www.google.com/act


semainierACT

**Responsable : **

* Derbel Dilel



* Tison Sophie



* Voge Marie-Emilie



**Intervenant : **

* Berveglieri N



* Bilasco M



* Derbel Dilel



* Fortin P



* Tison Sophie



* Voge Marie-Emilie



#### IIR
UE obligatoire


INITIATION À L'INNOVATION ET À LA RECHERCHE


ressourcesIIR : www.google.com/iir


ressourcesIIR2 : www.google.com/ressourcesIIR2


semainierIIR

**Responsable : **

* Le Pallec Xavier



**Intervenant : **

* Belmonte Romain



* Duchien Laurence



* Le Pallec Xavier



* Plénacoste Patricia



* Tarby Jean-Claude



### S2
#### ISI
UE obligatoire


Les objectifs de ce module sont les suivants : Connaitre les concepts principaux de sécurité (authentification, confidentalité, etc.) et les risques associés, connaitre les outils permettant de détecter les vulnérabilités logicielles (fuzzing, analyse statique, etc.) et savoir déployer des stratégies de mitigation pour limiter l’impact d’une vulnérabilité (séparation de privilèges, virtualisation, etc.)

**Responsable : **

* Grimaud Gilles



**Intervenant : **

#### IIHM
UE optionnelle


L'UE vise à faire prendre conscience aux étudiants l'importance de considérer les besoins et les caractéristiques des utilisateurs finaux dans le développement de systèmes interactifs. Le contenu se concentre sur la démarche de conception centrée utilisateurs et le maquettage. Tous les aspects liés aux développement d'interfaces graphiques interactives sont également étudiés..

**Responsable : **

* Pietrzak Thomas



**Intervenant : **

* Malacria Sylvain



# Master MIAGE

Centrée sur l'ingénierie des systèmes d'information, la MIAGE a pour vocation de former des informaticiens. Maîtrisant les technologies de l'information et les méthodes permettant de conduire un projet informatique depuis la spécification des besoins jusqu'à la mise en place. Sachant communiquer avec les partenaires de l'entreprise. Connaissant l'organisation des entreprises et leurs systèmes d'informationLe master MIAGE ne comporte qu'une seule spécialité en seconde année appelé Ingénierie de projets informatiques-nouvelles technologies (IPI-NT). Le master mention MIAGE comporte deux dispositifs : soit à temps plein à l'université (en formation "initiale"), soit par alternance, où les étudiants sont deux jours par semaine à l'université et trois jours en entreprise.

**Responsable : **

* Bilasco M




