/**
 * generated by Xtext 2.25.0
 */
package org.xtext.fil.fil;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fil</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.fil.fil.Fil#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.fil.fil.Fil#getDescription <em>Description</em>}</li>
 *   <li>{@link org.xtext.fil.fil.Fil#getResponsable <em>Responsable</em>}</li>
 *   <li>{@link org.xtext.fil.fil.Fil#getFormations <em>Formations</em>}</li>
 *   <li>{@link org.xtext.fil.fil.Fil#getPersonnes <em>Personnes</em>}</li>
 *   <li>{@link org.xtext.fil.fil.Fil#getUes <em>Ues</em>}</li>
 * </ul>
 *
 * @see org.xtext.fil.fil.FilPackage#getFil()
 * @model
 * @generated
 */
public interface Fil extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.fil.fil.FilPackage#getFil_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.fil.fil.Fil#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' attribute.
   * @see #setDescription(String)
   * @see org.xtext.fil.fil.FilPackage#getFil_Description()
   * @model
   * @generated
   */
  String getDescription();

  /**
   * Sets the value of the '{@link org.xtext.fil.fil.Fil#getDescription <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' attribute.
   * @see #getDescription()
   * @generated
   */
  void setDescription(String value);

  /**
   * Returns the value of the '<em><b>Responsable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Responsable</em>' reference.
   * @see #setResponsable(Personne)
   * @see org.xtext.fil.fil.FilPackage#getFil_Responsable()
   * @model
   * @generated
   */
  Personne getResponsable();

  /**
   * Sets the value of the '{@link org.xtext.fil.fil.Fil#getResponsable <em>Responsable</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Responsable</em>' reference.
   * @see #getResponsable()
   * @generated
   */
  void setResponsable(Personne value);

  /**
   * Returns the value of the '<em><b>Formations</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.fil.fil.Formation}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formations</em>' containment reference list.
   * @see org.xtext.fil.fil.FilPackage#getFil_Formations()
   * @model containment="true"
   * @generated
   */
  EList<Formation> getFormations();

  /**
   * Returns the value of the '<em><b>Personnes</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.fil.fil.Personne}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Personnes</em>' containment reference list.
   * @see org.xtext.fil.fil.FilPackage#getFil_Personnes()
   * @model containment="true"
   * @generated
   */
  EList<Personne> getPersonnes();

  /**
   * Returns the value of the '<em><b>Ues</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.fil.fil.UE}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ues</em>' containment reference list.
   * @see org.xtext.fil.fil.FilPackage#getFil_Ues()
   * @model containment="true"
   * @generated
   */
  EList<UE> getUes();

} // Fil
