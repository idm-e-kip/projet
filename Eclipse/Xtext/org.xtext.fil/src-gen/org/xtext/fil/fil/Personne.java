/**
 * generated by Xtext 2.25.0
 */
package org.xtext.fil.fil;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Personne</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.fil.fil.Personne#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.fil.fil.Personne#getPrenom <em>Prenom</em>}</li>
 * </ul>
 *
 * @see org.xtext.fil.fil.FilPackage#getPersonne()
 * @model
 * @generated
 */
public interface Personne extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.fil.fil.FilPackage#getPersonne_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.fil.fil.Personne#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Prenom</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prenom</em>' attribute.
   * @see #setPrenom(String)
   * @see org.xtext.fil.fil.FilPackage#getPersonne_Prenom()
   * @model
   * @generated
   */
  String getPrenom();

  /**
   * Sets the value of the '{@link org.xtext.fil.fil.Personne#getPrenom <em>Prenom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prenom</em>' attribute.
   * @see #getPrenom()
   * @generated
   */
  void setPrenom(String value);

} // Personne
