/**
 * generated by Xtext 2.25.0
 */
package org.xtext.fil.fil.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.fil.fil.FilPackage;
import org.xtext.fil.fil.Semainier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semainier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SemainierImpl extends ContenuImpl implements Semainier
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SemainierImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FilPackage.Literals.SEMAINIER;
  }

} //SemainierImpl
