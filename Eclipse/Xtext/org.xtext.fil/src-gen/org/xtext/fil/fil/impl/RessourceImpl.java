/**
 * generated by Xtext 2.25.0
 */
package org.xtext.fil.fil.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.fil.fil.FilPackage;
import org.xtext.fil.fil.Ressource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ressource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.fil.fil.impl.RessourceImpl#getUrl <em>Url</em>}</li>
 *   <li>{@link org.xtext.fil.fil.impl.RessourceImpl#getTaille <em>Taille</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RessourceImpl extends ContenuImpl implements Ressource
{
  /**
   * The default value of the '{@link #getUrl() <em>Url</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUrl()
   * @generated
   * @ordered
   */
  protected static final String URL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUrl() <em>Url</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUrl()
   * @generated
   * @ordered
   */
  protected String url = URL_EDEFAULT;

  /**
   * The default value of the '{@link #getTaille() <em>Taille</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTaille()
   * @generated
   * @ordered
   */
  protected static final int TAILLE_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getTaille() <em>Taille</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTaille()
   * @generated
   * @ordered
   */
  protected int taille = TAILLE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RessourceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FilPackage.Literals.RESSOURCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getUrl()
  {
    return url;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setUrl(String newUrl)
  {
    String oldUrl = url;
    url = newUrl;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FilPackage.RESSOURCE__URL, oldUrl, url));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int getTaille()
  {
    return taille;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setTaille(int newTaille)
  {
    int oldTaille = taille;
    taille = newTaille;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FilPackage.RESSOURCE__TAILLE, oldTaille, taille));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FilPackage.RESSOURCE__URL:
        return getUrl();
      case FilPackage.RESSOURCE__TAILLE:
        return getTaille();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FilPackage.RESSOURCE__URL:
        setUrl((String)newValue);
        return;
      case FilPackage.RESSOURCE__TAILLE:
        setTaille((Integer)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FilPackage.RESSOURCE__URL:
        setUrl(URL_EDEFAULT);
        return;
      case FilPackage.RESSOURCE__TAILLE:
        setTaille(TAILLE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FilPackage.RESSOURCE__URL:
        return URL_EDEFAULT == null ? url != null : !URL_EDEFAULT.equals(url);
      case FilPackage.RESSOURCE__TAILLE:
        return taille != TAILLE_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (url: ");
    result.append(url);
    result.append(", taille: ");
    result.append(taille);
    result.append(')');
    return result.toString();
  }

} //RessourceImpl
