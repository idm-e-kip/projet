package org.xtext.fil.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.fil.services.PortailGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPortailParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Fil'", "'{'", "'description'", "'responsable'", "'formations'", "','", "'}'", "'personnes'", "'ues'", "'Formation'", "'responsables'", "'('", "')'", "'parcours'", "'contenus'", "'semestre'", "'Personne'", "'prenom'", "'optionnelle'", "'UE'", "'intervenants'", "'Parcours'", "'semestres'", "'Semestre'", "'numero'", "'Description'", "'texte'", "'Ressource'", "'url'", "'taille'", "'Semainier'", "'-'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPortailParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPortailParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPortailParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPortail.g"; }



     	private PortailGrammarAccess grammarAccess;

        public InternalPortailParser(TokenStream input, PortailGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Fil";
       	}

       	@Override
       	protected PortailGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFil"
    // InternalPortail.g:64:1: entryRuleFil returns [EObject current=null] : iv_ruleFil= ruleFil EOF ;
    public final EObject entryRuleFil() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFil = null;


        try {
            // InternalPortail.g:64:44: (iv_ruleFil= ruleFil EOF )
            // InternalPortail.g:65:2: iv_ruleFil= ruleFil EOF
            {
             newCompositeNode(grammarAccess.getFilRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFil=ruleFil();

            state._fsp--;

             current =iv_ruleFil; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFil"


    // $ANTLR start "ruleFil"
    // InternalPortail.g:71:1: ruleFil returns [EObject current=null] : ( () otherlv_1= 'Fil' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'responsable' ( ( ruleEString ) ) )? (otherlv_8= 'formations' otherlv_9= '{' ( (lv_formations_10_0= ruleFormation ) ) (otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) ) )* otherlv_13= '}' )? (otherlv_14= 'personnes' otherlv_15= '{' ( (lv_personnes_16_0= rulePersonne ) ) (otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= '}' )? (otherlv_20= 'ues' otherlv_21= '{' ( (lv_ues_22_0= ruleUE ) ) (otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) ) )* otherlv_25= '}' )? otherlv_26= '}' ) ;
    public final EObject ruleFil() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_5_0 = null;

        EObject lv_formations_10_0 = null;

        EObject lv_formations_12_0 = null;

        EObject lv_personnes_16_0 = null;

        EObject lv_personnes_18_0 = null;

        EObject lv_ues_22_0 = null;

        EObject lv_ues_24_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:77:2: ( ( () otherlv_1= 'Fil' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'responsable' ( ( ruleEString ) ) )? (otherlv_8= 'formations' otherlv_9= '{' ( (lv_formations_10_0= ruleFormation ) ) (otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) ) )* otherlv_13= '}' )? (otherlv_14= 'personnes' otherlv_15= '{' ( (lv_personnes_16_0= rulePersonne ) ) (otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= '}' )? (otherlv_20= 'ues' otherlv_21= '{' ( (lv_ues_22_0= ruleUE ) ) (otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) ) )* otherlv_25= '}' )? otherlv_26= '}' ) )
            // InternalPortail.g:78:2: ( () otherlv_1= 'Fil' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'responsable' ( ( ruleEString ) ) )? (otherlv_8= 'formations' otherlv_9= '{' ( (lv_formations_10_0= ruleFormation ) ) (otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) ) )* otherlv_13= '}' )? (otherlv_14= 'personnes' otherlv_15= '{' ( (lv_personnes_16_0= rulePersonne ) ) (otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= '}' )? (otherlv_20= 'ues' otherlv_21= '{' ( (lv_ues_22_0= ruleUE ) ) (otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) ) )* otherlv_25= '}' )? otherlv_26= '}' )
            {
            // InternalPortail.g:78:2: ( () otherlv_1= 'Fil' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'responsable' ( ( ruleEString ) ) )? (otherlv_8= 'formations' otherlv_9= '{' ( (lv_formations_10_0= ruleFormation ) ) (otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) ) )* otherlv_13= '}' )? (otherlv_14= 'personnes' otherlv_15= '{' ( (lv_personnes_16_0= rulePersonne ) ) (otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= '}' )? (otherlv_20= 'ues' otherlv_21= '{' ( (lv_ues_22_0= ruleUE ) ) (otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) ) )* otherlv_25= '}' )? otherlv_26= '}' )
            // InternalPortail.g:79:3: () otherlv_1= 'Fil' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'responsable' ( ( ruleEString ) ) )? (otherlv_8= 'formations' otherlv_9= '{' ( (lv_formations_10_0= ruleFormation ) ) (otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) ) )* otherlv_13= '}' )? (otherlv_14= 'personnes' otherlv_15= '{' ( (lv_personnes_16_0= rulePersonne ) ) (otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= '}' )? (otherlv_20= 'ues' otherlv_21= '{' ( (lv_ues_22_0= ruleUE ) ) (otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) ) )* otherlv_25= '}' )? otherlv_26= '}'
            {
            // InternalPortail.g:79:3: ()
            // InternalPortail.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFilAccess().getFilAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getFilAccess().getFilKeyword_1());
            		
            // InternalPortail.g:90:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPortail.g:91:4: (lv_name_2_0= ruleEString )
            {
            // InternalPortail.g:91:4: (lv_name_2_0= ruleEString )
            // InternalPortail.g:92:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getFilAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFilRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.xtext.fil.Portail.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getFilAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPortail.g:113:3: (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==13) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalPortail.g:114:4: otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getFilAccess().getDescriptionKeyword_4_0());
                    			
                    // InternalPortail.g:118:4: ( (lv_description_5_0= ruleEString ) )
                    // InternalPortail.g:119:5: (lv_description_5_0= ruleEString )
                    {
                    // InternalPortail.g:119:5: (lv_description_5_0= ruleEString )
                    // InternalPortail.g:120:6: lv_description_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getFilAccess().getDescriptionEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_description_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFilRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_5_0,
                    							"org.xtext.fil.Portail.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPortail.g:138:3: (otherlv_6= 'responsable' ( ( ruleEString ) ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalPortail.g:139:4: otherlv_6= 'responsable' ( ( ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,14,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getFilAccess().getResponsableKeyword_5_0());
                    			
                    // InternalPortail.g:143:4: ( ( ruleEString ) )
                    // InternalPortail.g:144:5: ( ruleEString )
                    {
                    // InternalPortail.g:144:5: ( ruleEString )
                    // InternalPortail.g:145:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFilRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getFilAccess().getResponsablePersonneCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_7);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPortail.g:160:3: (otherlv_8= 'formations' otherlv_9= '{' ( (lv_formations_10_0= ruleFormation ) ) (otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) ) )* otherlv_13= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalPortail.g:161:4: otherlv_8= 'formations' otherlv_9= '{' ( (lv_formations_10_0= ruleFormation ) ) (otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) ) )* otherlv_13= '}'
                    {
                    otherlv_8=(Token)match(input,15,FOLLOW_4); 

                    				newLeafNode(otherlv_8, grammarAccess.getFilAccess().getFormationsKeyword_6_0());
                    			
                    otherlv_9=(Token)match(input,12,FOLLOW_8); 

                    				newLeafNode(otherlv_9, grammarAccess.getFilAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalPortail.g:169:4: ( (lv_formations_10_0= ruleFormation ) )
                    // InternalPortail.g:170:5: (lv_formations_10_0= ruleFormation )
                    {
                    // InternalPortail.g:170:5: (lv_formations_10_0= ruleFormation )
                    // InternalPortail.g:171:6: lv_formations_10_0= ruleFormation
                    {

                    						newCompositeNode(grammarAccess.getFilAccess().getFormationsFormationParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_formations_10_0=ruleFormation();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFilRule());
                    						}
                    						add(
                    							current,
                    							"formations",
                    							lv_formations_10_0,
                    							"org.xtext.fil.Portail.Formation");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:188:4: (otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==16) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalPortail.g:189:5: otherlv_11= ',' ( (lv_formations_12_0= ruleFormation ) )
                    	    {
                    	    otherlv_11=(Token)match(input,16,FOLLOW_8); 

                    	    					newLeafNode(otherlv_11, grammarAccess.getFilAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalPortail.g:193:5: ( (lv_formations_12_0= ruleFormation ) )
                    	    // InternalPortail.g:194:6: (lv_formations_12_0= ruleFormation )
                    	    {
                    	    // InternalPortail.g:194:6: (lv_formations_12_0= ruleFormation )
                    	    // InternalPortail.g:195:7: lv_formations_12_0= ruleFormation
                    	    {

                    	    							newCompositeNode(grammarAccess.getFilAccess().getFormationsFormationParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_formations_12_0=ruleFormation();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFilRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"formations",
                    	    								lv_formations_12_0,
                    	    								"org.xtext.fil.Portail.Formation");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,17,FOLLOW_10); 

                    				newLeafNode(otherlv_13, grammarAccess.getFilAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:218:3: (otherlv_14= 'personnes' otherlv_15= '{' ( (lv_personnes_16_0= rulePersonne ) ) (otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalPortail.g:219:4: otherlv_14= 'personnes' otherlv_15= '{' ( (lv_personnes_16_0= rulePersonne ) ) (otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= '}'
                    {
                    otherlv_14=(Token)match(input,18,FOLLOW_4); 

                    				newLeafNode(otherlv_14, grammarAccess.getFilAccess().getPersonnesKeyword_7_0());
                    			
                    otherlv_15=(Token)match(input,12,FOLLOW_11); 

                    				newLeafNode(otherlv_15, grammarAccess.getFilAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalPortail.g:227:4: ( (lv_personnes_16_0= rulePersonne ) )
                    // InternalPortail.g:228:5: (lv_personnes_16_0= rulePersonne )
                    {
                    // InternalPortail.g:228:5: (lv_personnes_16_0= rulePersonne )
                    // InternalPortail.g:229:6: lv_personnes_16_0= rulePersonne
                    {

                    						newCompositeNode(grammarAccess.getFilAccess().getPersonnesPersonneParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_personnes_16_0=rulePersonne();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFilRule());
                    						}
                    						add(
                    							current,
                    							"personnes",
                    							lv_personnes_16_0,
                    							"org.xtext.fil.Portail.Personne");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:246:4: (otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==16) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalPortail.g:247:5: otherlv_17= ',' ( (lv_personnes_18_0= rulePersonne ) )
                    	    {
                    	    otherlv_17=(Token)match(input,16,FOLLOW_11); 

                    	    					newLeafNode(otherlv_17, grammarAccess.getFilAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalPortail.g:251:5: ( (lv_personnes_18_0= rulePersonne ) )
                    	    // InternalPortail.g:252:6: (lv_personnes_18_0= rulePersonne )
                    	    {
                    	    // InternalPortail.g:252:6: (lv_personnes_18_0= rulePersonne )
                    	    // InternalPortail.g:253:7: lv_personnes_18_0= rulePersonne
                    	    {

                    	    							newCompositeNode(grammarAccess.getFilAccess().getPersonnesPersonneParserRuleCall_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_personnes_18_0=rulePersonne();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFilRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"personnes",
                    	    								lv_personnes_18_0,
                    	    								"org.xtext.fil.Portail.Personne");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_19=(Token)match(input,17,FOLLOW_12); 

                    				newLeafNode(otherlv_19, grammarAccess.getFilAccess().getRightCurlyBracketKeyword_7_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:276:3: (otherlv_20= 'ues' otherlv_21= '{' ( (lv_ues_22_0= ruleUE ) ) (otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) ) )* otherlv_25= '}' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==19) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalPortail.g:277:4: otherlv_20= 'ues' otherlv_21= '{' ( (lv_ues_22_0= ruleUE ) ) (otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) ) )* otherlv_25= '}'
                    {
                    otherlv_20=(Token)match(input,19,FOLLOW_4); 

                    				newLeafNode(otherlv_20, grammarAccess.getFilAccess().getUesKeyword_8_0());
                    			
                    otherlv_21=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_21, grammarAccess.getFilAccess().getLeftCurlyBracketKeyword_8_1());
                    			
                    // InternalPortail.g:285:4: ( (lv_ues_22_0= ruleUE ) )
                    // InternalPortail.g:286:5: (lv_ues_22_0= ruleUE )
                    {
                    // InternalPortail.g:286:5: (lv_ues_22_0= ruleUE )
                    // InternalPortail.g:287:6: lv_ues_22_0= ruleUE
                    {

                    						newCompositeNode(grammarAccess.getFilAccess().getUesUEParserRuleCall_8_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_ues_22_0=ruleUE();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFilRule());
                    						}
                    						add(
                    							current,
                    							"ues",
                    							lv_ues_22_0,
                    							"org.xtext.fil.Portail.UE");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:304:4: (otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) ) )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==16) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalPortail.g:305:5: otherlv_23= ',' ( (lv_ues_24_0= ruleUE ) )
                    	    {
                    	    otherlv_23=(Token)match(input,16,FOLLOW_13); 

                    	    					newLeafNode(otherlv_23, grammarAccess.getFilAccess().getCommaKeyword_8_3_0());
                    	    				
                    	    // InternalPortail.g:309:5: ( (lv_ues_24_0= ruleUE ) )
                    	    // InternalPortail.g:310:6: (lv_ues_24_0= ruleUE )
                    	    {
                    	    // InternalPortail.g:310:6: (lv_ues_24_0= ruleUE )
                    	    // InternalPortail.g:311:7: lv_ues_24_0= ruleUE
                    	    {

                    	    							newCompositeNode(grammarAccess.getFilAccess().getUesUEParserRuleCall_8_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_ues_24_0=ruleUE();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFilRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"ues",
                    	    								lv_ues_24_0,
                    	    								"org.xtext.fil.Portail.UE");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    otherlv_25=(Token)match(input,17,FOLLOW_14); 

                    				newLeafNode(otherlv_25, grammarAccess.getFilAccess().getRightCurlyBracketKeyword_8_4());
                    			

                    }
                    break;

            }

            otherlv_26=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_26, grammarAccess.getFilAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFil"


    // $ANTLR start "entryRuleContenu"
    // InternalPortail.g:342:1: entryRuleContenu returns [EObject current=null] : iv_ruleContenu= ruleContenu EOF ;
    public final EObject entryRuleContenu() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContenu = null;


        try {
            // InternalPortail.g:342:48: (iv_ruleContenu= ruleContenu EOF )
            // InternalPortail.g:343:2: iv_ruleContenu= ruleContenu EOF
            {
             newCompositeNode(grammarAccess.getContenuRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleContenu=ruleContenu();

            state._fsp--;

             current =iv_ruleContenu; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContenu"


    // $ANTLR start "ruleContenu"
    // InternalPortail.g:349:1: ruleContenu returns [EObject current=null] : (this_Description_0= ruleDescription | this_Ressource_1= ruleRessource | this_Semainier_2= ruleSemainier ) ;
    public final EObject ruleContenu() throws RecognitionException {
        EObject current = null;

        EObject this_Description_0 = null;

        EObject this_Ressource_1 = null;

        EObject this_Semainier_2 = null;



        	enterRule();

        try {
            // InternalPortail.g:355:2: ( (this_Description_0= ruleDescription | this_Ressource_1= ruleRessource | this_Semainier_2= ruleSemainier ) )
            // InternalPortail.g:356:2: (this_Description_0= ruleDescription | this_Ressource_1= ruleRessource | this_Semainier_2= ruleSemainier )
            {
            // InternalPortail.g:356:2: (this_Description_0= ruleDescription | this_Ressource_1= ruleRessource | this_Semainier_2= ruleSemainier )
            int alt9=3;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt9=1;
                }
                break;
            case 38:
                {
                alt9=2;
                }
                break;
            case 41:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalPortail.g:357:3: this_Description_0= ruleDescription
                    {

                    			newCompositeNode(grammarAccess.getContenuAccess().getDescriptionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Description_0=ruleDescription();

                    state._fsp--;


                    			current = this_Description_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPortail.g:366:3: this_Ressource_1= ruleRessource
                    {

                    			newCompositeNode(grammarAccess.getContenuAccess().getRessourceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Ressource_1=ruleRessource();

                    state._fsp--;


                    			current = this_Ressource_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalPortail.g:375:3: this_Semainier_2= ruleSemainier
                    {

                    			newCompositeNode(grammarAccess.getContenuAccess().getSemainierParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Semainier_2=ruleSemainier();

                    state._fsp--;


                    			current = this_Semainier_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContenu"


    // $ANTLR start "entryRuleFormation"
    // InternalPortail.g:387:1: entryRuleFormation returns [EObject current=null] : iv_ruleFormation= ruleFormation EOF ;
    public final EObject entryRuleFormation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormation = null;


        try {
            // InternalPortail.g:387:50: (iv_ruleFormation= ruleFormation EOF )
            // InternalPortail.g:388:2: iv_ruleFormation= ruleFormation EOF
            {
             newCompositeNode(grammarAccess.getFormationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFormation=ruleFormation();

            state._fsp--;

             current =iv_ruleFormation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormation"


    // $ANTLR start "ruleFormation"
    // InternalPortail.g:394:1: ruleFormation returns [EObject current=null] : ( () otherlv_1= 'Formation' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'parcours' otherlv_11= '{' ( (lv_parcours_12_0= ruleParcours ) ) (otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) ) )* otherlv_15= '}' )? (otherlv_16= 'contenus' otherlv_17= '{' ( (lv_contenus_18_0= ruleContenu ) ) (otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) ) )* otherlv_21= '}' )? (otherlv_22= 'semestre' otherlv_23= '{' ( (lv_semestre_24_0= ruleSemestre ) ) (otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) ) )* otherlv_27= '}' )? otherlv_28= '}' ) ;
    public final EObject ruleFormation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_parcours_12_0 = null;

        EObject lv_parcours_14_0 = null;

        EObject lv_contenus_18_0 = null;

        EObject lv_contenus_20_0 = null;

        EObject lv_semestre_24_0 = null;

        EObject lv_semestre_26_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:400:2: ( ( () otherlv_1= 'Formation' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'parcours' otherlv_11= '{' ( (lv_parcours_12_0= ruleParcours ) ) (otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) ) )* otherlv_15= '}' )? (otherlv_16= 'contenus' otherlv_17= '{' ( (lv_contenus_18_0= ruleContenu ) ) (otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) ) )* otherlv_21= '}' )? (otherlv_22= 'semestre' otherlv_23= '{' ( (lv_semestre_24_0= ruleSemestre ) ) (otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) ) )* otherlv_27= '}' )? otherlv_28= '}' ) )
            // InternalPortail.g:401:2: ( () otherlv_1= 'Formation' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'parcours' otherlv_11= '{' ( (lv_parcours_12_0= ruleParcours ) ) (otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) ) )* otherlv_15= '}' )? (otherlv_16= 'contenus' otherlv_17= '{' ( (lv_contenus_18_0= ruleContenu ) ) (otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) ) )* otherlv_21= '}' )? (otherlv_22= 'semestre' otherlv_23= '{' ( (lv_semestre_24_0= ruleSemestre ) ) (otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) ) )* otherlv_27= '}' )? otherlv_28= '}' )
            {
            // InternalPortail.g:401:2: ( () otherlv_1= 'Formation' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'parcours' otherlv_11= '{' ( (lv_parcours_12_0= ruleParcours ) ) (otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) ) )* otherlv_15= '}' )? (otherlv_16= 'contenus' otherlv_17= '{' ( (lv_contenus_18_0= ruleContenu ) ) (otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) ) )* otherlv_21= '}' )? (otherlv_22= 'semestre' otherlv_23= '{' ( (lv_semestre_24_0= ruleSemestre ) ) (otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) ) )* otherlv_27= '}' )? otherlv_28= '}' )
            // InternalPortail.g:402:3: () otherlv_1= 'Formation' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'parcours' otherlv_11= '{' ( (lv_parcours_12_0= ruleParcours ) ) (otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) ) )* otherlv_15= '}' )? (otherlv_16= 'contenus' otherlv_17= '{' ( (lv_contenus_18_0= ruleContenu ) ) (otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) ) )* otherlv_21= '}' )? (otherlv_22= 'semestre' otherlv_23= '{' ( (lv_semestre_24_0= ruleSemestre ) ) (otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) ) )* otherlv_27= '}' )? otherlv_28= '}'
            {
            // InternalPortail.g:402:3: ()
            // InternalPortail.g:403:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFormationAccess().getFormationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getFormationAccess().getFormationKeyword_1());
            		
            // InternalPortail.g:413:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPortail.g:414:4: (lv_name_2_0= ruleEString )
            {
            // InternalPortail.g:414:4: (lv_name_2_0= ruleEString )
            // InternalPortail.g:415:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getFormationAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFormationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.xtext.fil.Portail.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_3, grammarAccess.getFormationAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPortail.g:436:3: (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==21) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalPortail.g:437:4: otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,21,FOLLOW_16); 

                    				newLeafNode(otherlv_4, grammarAccess.getFormationAccess().getResponsablesKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,22,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getFormationAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalPortail.g:445:4: ( ( ruleEString ) )
                    // InternalPortail.g:446:5: ( ruleEString )
                    {
                    // InternalPortail.g:446:5: ( ruleEString )
                    // InternalPortail.g:447:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFormationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getFormationAccess().getResponsablesPersonneCrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_17);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:461:4: (otherlv_7= ',' ( ( ruleEString ) ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==16) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalPortail.g:462:5: otherlv_7= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_7=(Token)match(input,16,FOLLOW_3); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getFormationAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalPortail.g:466:5: ( ( ruleEString ) )
                    	    // InternalPortail.g:467:6: ( ruleEString )
                    	    {
                    	    // InternalPortail.g:467:6: ( ruleEString )
                    	    // InternalPortail.g:468:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getFormationRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getFormationAccess().getResponsablesPersonneCrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_17);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,23,FOLLOW_18); 

                    				newLeafNode(otherlv_9, grammarAccess.getFormationAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:488:3: (otherlv_10= 'parcours' otherlv_11= '{' ( (lv_parcours_12_0= ruleParcours ) ) (otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) ) )* otherlv_15= '}' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==24) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalPortail.g:489:4: otherlv_10= 'parcours' otherlv_11= '{' ( (lv_parcours_12_0= ruleParcours ) ) (otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) ) )* otherlv_15= '}'
                    {
                    otherlv_10=(Token)match(input,24,FOLLOW_4); 

                    				newLeafNode(otherlv_10, grammarAccess.getFormationAccess().getParcoursKeyword_5_0());
                    			
                    otherlv_11=(Token)match(input,12,FOLLOW_19); 

                    				newLeafNode(otherlv_11, grammarAccess.getFormationAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalPortail.g:497:4: ( (lv_parcours_12_0= ruleParcours ) )
                    // InternalPortail.g:498:5: (lv_parcours_12_0= ruleParcours )
                    {
                    // InternalPortail.g:498:5: (lv_parcours_12_0= ruleParcours )
                    // InternalPortail.g:499:6: lv_parcours_12_0= ruleParcours
                    {

                    						newCompositeNode(grammarAccess.getFormationAccess().getParcoursParcoursParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_parcours_12_0=ruleParcours();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFormationRule());
                    						}
                    						add(
                    							current,
                    							"parcours",
                    							lv_parcours_12_0,
                    							"org.xtext.fil.Portail.Parcours");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:516:4: (otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==16) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalPortail.g:517:5: otherlv_13= ',' ( (lv_parcours_14_0= ruleParcours ) )
                    	    {
                    	    otherlv_13=(Token)match(input,16,FOLLOW_19); 

                    	    					newLeafNode(otherlv_13, grammarAccess.getFormationAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalPortail.g:521:5: ( (lv_parcours_14_0= ruleParcours ) )
                    	    // InternalPortail.g:522:6: (lv_parcours_14_0= ruleParcours )
                    	    {
                    	    // InternalPortail.g:522:6: (lv_parcours_14_0= ruleParcours )
                    	    // InternalPortail.g:523:7: lv_parcours_14_0= ruleParcours
                    	    {

                    	    							newCompositeNode(grammarAccess.getFormationAccess().getParcoursParcoursParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_parcours_14_0=ruleParcours();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFormationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parcours",
                    	    								lv_parcours_14_0,
                    	    								"org.xtext.fil.Portail.Parcours");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,17,FOLLOW_20); 

                    				newLeafNode(otherlv_15, grammarAccess.getFormationAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:546:3: (otherlv_16= 'contenus' otherlv_17= '{' ( (lv_contenus_18_0= ruleContenu ) ) (otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) ) )* otherlv_21= '}' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==25) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalPortail.g:547:4: otherlv_16= 'contenus' otherlv_17= '{' ( (lv_contenus_18_0= ruleContenu ) ) (otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,25,FOLLOW_4); 

                    				newLeafNode(otherlv_16, grammarAccess.getFormationAccess().getContenusKeyword_6_0());
                    			
                    otherlv_17=(Token)match(input,12,FOLLOW_21); 

                    				newLeafNode(otherlv_17, grammarAccess.getFormationAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalPortail.g:555:4: ( (lv_contenus_18_0= ruleContenu ) )
                    // InternalPortail.g:556:5: (lv_contenus_18_0= ruleContenu )
                    {
                    // InternalPortail.g:556:5: (lv_contenus_18_0= ruleContenu )
                    // InternalPortail.g:557:6: lv_contenus_18_0= ruleContenu
                    {

                    						newCompositeNode(grammarAccess.getFormationAccess().getContenusContenuParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_contenus_18_0=ruleContenu();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFormationRule());
                    						}
                    						add(
                    							current,
                    							"contenus",
                    							lv_contenus_18_0,
                    							"org.xtext.fil.Portail.Contenu");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:574:4: (otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==16) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalPortail.g:575:5: otherlv_19= ',' ( (lv_contenus_20_0= ruleContenu ) )
                    	    {
                    	    otherlv_19=(Token)match(input,16,FOLLOW_21); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getFormationAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalPortail.g:579:5: ( (lv_contenus_20_0= ruleContenu ) )
                    	    // InternalPortail.g:580:6: (lv_contenus_20_0= ruleContenu )
                    	    {
                    	    // InternalPortail.g:580:6: (lv_contenus_20_0= ruleContenu )
                    	    // InternalPortail.g:581:7: lv_contenus_20_0= ruleContenu
                    	    {

                    	    							newCompositeNode(grammarAccess.getFormationAccess().getContenusContenuParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_contenus_20_0=ruleContenu();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFormationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"contenus",
                    	    								lv_contenus_20_0,
                    	    								"org.xtext.fil.Portail.Contenu");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,17,FOLLOW_22); 

                    				newLeafNode(otherlv_21, grammarAccess.getFormationAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:604:3: (otherlv_22= 'semestre' otherlv_23= '{' ( (lv_semestre_24_0= ruleSemestre ) ) (otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) ) )* otherlv_27= '}' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==26) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalPortail.g:605:4: otherlv_22= 'semestre' otherlv_23= '{' ( (lv_semestre_24_0= ruleSemestre ) ) (otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) ) )* otherlv_27= '}'
                    {
                    otherlv_22=(Token)match(input,26,FOLLOW_4); 

                    				newLeafNode(otherlv_22, grammarAccess.getFormationAccess().getSemestreKeyword_7_0());
                    			
                    otherlv_23=(Token)match(input,12,FOLLOW_23); 

                    				newLeafNode(otherlv_23, grammarAccess.getFormationAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalPortail.g:613:4: ( (lv_semestre_24_0= ruleSemestre ) )
                    // InternalPortail.g:614:5: (lv_semestre_24_0= ruleSemestre )
                    {
                    // InternalPortail.g:614:5: (lv_semestre_24_0= ruleSemestre )
                    // InternalPortail.g:615:6: lv_semestre_24_0= ruleSemestre
                    {

                    						newCompositeNode(grammarAccess.getFormationAccess().getSemestreSemestreParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_semestre_24_0=ruleSemestre();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFormationRule());
                    						}
                    						add(
                    							current,
                    							"semestre",
                    							lv_semestre_24_0,
                    							"org.xtext.fil.Portail.Semestre");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:632:4: (otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==16) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // InternalPortail.g:633:5: otherlv_25= ',' ( (lv_semestre_26_0= ruleSemestre ) )
                    	    {
                    	    otherlv_25=(Token)match(input,16,FOLLOW_23); 

                    	    					newLeafNode(otherlv_25, grammarAccess.getFormationAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalPortail.g:637:5: ( (lv_semestre_26_0= ruleSemestre ) )
                    	    // InternalPortail.g:638:6: (lv_semestre_26_0= ruleSemestre )
                    	    {
                    	    // InternalPortail.g:638:6: (lv_semestre_26_0= ruleSemestre )
                    	    // InternalPortail.g:639:7: lv_semestre_26_0= ruleSemestre
                    	    {

                    	    							newCompositeNode(grammarAccess.getFormationAccess().getSemestreSemestreParserRuleCall_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_semestre_26_0=ruleSemestre();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFormationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"semestre",
                    	    								lv_semestre_26_0,
                    	    								"org.xtext.fil.Portail.Semestre");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);

                    otherlv_27=(Token)match(input,17,FOLLOW_14); 

                    				newLeafNode(otherlv_27, grammarAccess.getFormationAccess().getRightCurlyBracketKeyword_7_4());
                    			

                    }
                    break;

            }

            otherlv_28=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_28, grammarAccess.getFormationAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormation"


    // $ANTLR start "entryRulePersonne"
    // InternalPortail.g:670:1: entryRulePersonne returns [EObject current=null] : iv_rulePersonne= rulePersonne EOF ;
    public final EObject entryRulePersonne() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePersonne = null;


        try {
            // InternalPortail.g:670:49: (iv_rulePersonne= rulePersonne EOF )
            // InternalPortail.g:671:2: iv_rulePersonne= rulePersonne EOF
            {
             newCompositeNode(grammarAccess.getPersonneRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePersonne=rulePersonne();

            state._fsp--;

             current =iv_rulePersonne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePersonne"


    // $ANTLR start "rulePersonne"
    // InternalPortail.g:677:1: rulePersonne returns [EObject current=null] : ( () otherlv_1= 'Personne' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'prenom' ( (lv_prenom_5_0= ruleEString ) ) )? otherlv_6= '}' ) ;
    public final EObject rulePersonne() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_prenom_5_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:683:2: ( ( () otherlv_1= 'Personne' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'prenom' ( (lv_prenom_5_0= ruleEString ) ) )? otherlv_6= '}' ) )
            // InternalPortail.g:684:2: ( () otherlv_1= 'Personne' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'prenom' ( (lv_prenom_5_0= ruleEString ) ) )? otherlv_6= '}' )
            {
            // InternalPortail.g:684:2: ( () otherlv_1= 'Personne' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'prenom' ( (lv_prenom_5_0= ruleEString ) ) )? otherlv_6= '}' )
            // InternalPortail.g:685:3: () otherlv_1= 'Personne' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'prenom' ( (lv_prenom_5_0= ruleEString ) ) )? otherlv_6= '}'
            {
            // InternalPortail.g:685:3: ()
            // InternalPortail.g:686:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPersonneAccess().getPersonneAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPersonneAccess().getPersonneKeyword_1());
            		
            // InternalPortail.g:696:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPortail.g:697:4: (lv_name_2_0= ruleEString )
            {
            // InternalPortail.g:697:4: (lv_name_2_0= ruleEString )
            // InternalPortail.g:698:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPersonneAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPersonneRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.xtext.fil.Portail.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_24); 

            			newLeafNode(otherlv_3, grammarAccess.getPersonneAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPortail.g:719:3: (otherlv_4= 'prenom' ( (lv_prenom_5_0= ruleEString ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==28) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalPortail.g:720:4: otherlv_4= 'prenom' ( (lv_prenom_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,28,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getPersonneAccess().getPrenomKeyword_4_0());
                    			
                    // InternalPortail.g:724:4: ( (lv_prenom_5_0= ruleEString ) )
                    // InternalPortail.g:725:5: (lv_prenom_5_0= ruleEString )
                    {
                    // InternalPortail.g:725:5: (lv_prenom_5_0= ruleEString )
                    // InternalPortail.g:726:6: lv_prenom_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPersonneAccess().getPrenomEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_prenom_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPersonneRule());
                    						}
                    						set(
                    							current,
                    							"prenom",
                    							lv_prenom_5_0,
                    							"org.xtext.fil.Portail.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getPersonneAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePersonne"


    // $ANTLR start "entryRuleUE"
    // InternalPortail.g:752:1: entryRuleUE returns [EObject current=null] : iv_ruleUE= ruleUE EOF ;
    public final EObject entryRuleUE() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUE = null;


        try {
            // InternalPortail.g:752:43: (iv_ruleUE= ruleUE EOF )
            // InternalPortail.g:753:2: iv_ruleUE= ruleUE EOF
            {
             newCompositeNode(grammarAccess.getUERule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUE=ruleUE();

            state._fsp--;

             current =iv_ruleUE; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUE"


    // $ANTLR start "ruleUE"
    // InternalPortail.g:759:1: ruleUE returns [EObject current=null] : ( () ( (lv_optionnelle_1_0= 'optionnelle' ) )? otherlv_2= 'UE' ( (lv_name_3_0= ruleEString ) ) otherlv_4= '{' (otherlv_5= 'intervenants' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? (otherlv_11= 'responsables' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'contenus' otherlv_18= '{' ( (lv_contenus_19_0= ruleContenu ) ) (otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) ) )* otherlv_22= '}' )? otherlv_23= '}' ) ;
    public final EObject ruleUE() throws RecognitionException {
        EObject current = null;

        Token lv_optionnelle_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;

        EObject lv_contenus_19_0 = null;

        EObject lv_contenus_21_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:765:2: ( ( () ( (lv_optionnelle_1_0= 'optionnelle' ) )? otherlv_2= 'UE' ( (lv_name_3_0= ruleEString ) ) otherlv_4= '{' (otherlv_5= 'intervenants' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? (otherlv_11= 'responsables' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'contenus' otherlv_18= '{' ( (lv_contenus_19_0= ruleContenu ) ) (otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) ) )* otherlv_22= '}' )? otherlv_23= '}' ) )
            // InternalPortail.g:766:2: ( () ( (lv_optionnelle_1_0= 'optionnelle' ) )? otherlv_2= 'UE' ( (lv_name_3_0= ruleEString ) ) otherlv_4= '{' (otherlv_5= 'intervenants' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? (otherlv_11= 'responsables' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'contenus' otherlv_18= '{' ( (lv_contenus_19_0= ruleContenu ) ) (otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) ) )* otherlv_22= '}' )? otherlv_23= '}' )
            {
            // InternalPortail.g:766:2: ( () ( (lv_optionnelle_1_0= 'optionnelle' ) )? otherlv_2= 'UE' ( (lv_name_3_0= ruleEString ) ) otherlv_4= '{' (otherlv_5= 'intervenants' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? (otherlv_11= 'responsables' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'contenus' otherlv_18= '{' ( (lv_contenus_19_0= ruleContenu ) ) (otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) ) )* otherlv_22= '}' )? otherlv_23= '}' )
            // InternalPortail.g:767:3: () ( (lv_optionnelle_1_0= 'optionnelle' ) )? otherlv_2= 'UE' ( (lv_name_3_0= ruleEString ) ) otherlv_4= '{' (otherlv_5= 'intervenants' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? (otherlv_11= 'responsables' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'contenus' otherlv_18= '{' ( (lv_contenus_19_0= ruleContenu ) ) (otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) ) )* otherlv_22= '}' )? otherlv_23= '}'
            {
            // InternalPortail.g:767:3: ()
            // InternalPortail.g:768:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUEAccess().getUEAction_0(),
            					current);
            			

            }

            // InternalPortail.g:774:3: ( (lv_optionnelle_1_0= 'optionnelle' ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==29) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalPortail.g:775:4: (lv_optionnelle_1_0= 'optionnelle' )
                    {
                    // InternalPortail.g:775:4: (lv_optionnelle_1_0= 'optionnelle' )
                    // InternalPortail.g:776:5: lv_optionnelle_1_0= 'optionnelle'
                    {
                    lv_optionnelle_1_0=(Token)match(input,29,FOLLOW_25); 

                    					newLeafNode(lv_optionnelle_1_0, grammarAccess.getUEAccess().getOptionnelleOptionnelleKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getUERule());
                    					}
                    					setWithLastConsumed(current, "optionnelle", lv_optionnelle_1_0 != null, "optionnelle");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,30,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getUEAccess().getUEKeyword_2());
            		
            // InternalPortail.g:792:3: ( (lv_name_3_0= ruleEString ) )
            // InternalPortail.g:793:4: (lv_name_3_0= ruleEString )
            {
            // InternalPortail.g:793:4: (lv_name_3_0= ruleEString )
            // InternalPortail.g:794:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getUEAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUERule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"org.xtext.fil.Portail.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,12,FOLLOW_26); 

            			newLeafNode(otherlv_4, grammarAccess.getUEAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalPortail.g:815:3: (otherlv_5= 'intervenants' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==31) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalPortail.g:816:4: otherlv_5= 'intervenants' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')'
                    {
                    otherlv_5=(Token)match(input,31,FOLLOW_16); 

                    				newLeafNode(otherlv_5, grammarAccess.getUEAccess().getIntervenantsKeyword_5_0());
                    			
                    otherlv_6=(Token)match(input,22,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getUEAccess().getLeftParenthesisKeyword_5_1());
                    			
                    // InternalPortail.g:824:4: ( ( ruleEString ) )
                    // InternalPortail.g:825:5: ( ruleEString )
                    {
                    // InternalPortail.g:825:5: ( ruleEString )
                    // InternalPortail.g:826:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getUERule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getUEAccess().getIntervenantsPersonneCrossReference_5_2_0());
                    					
                    pushFollow(FOLLOW_17);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:840:4: (otherlv_8= ',' ( ( ruleEString ) ) )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==16) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalPortail.g:841:5: otherlv_8= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_8=(Token)match(input,16,FOLLOW_3); 

                    	    					newLeafNode(otherlv_8, grammarAccess.getUEAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalPortail.g:845:5: ( ( ruleEString ) )
                    	    // InternalPortail.g:846:6: ( ruleEString )
                    	    {
                    	    // InternalPortail.g:846:6: ( ruleEString )
                    	    // InternalPortail.g:847:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getUERule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getUEAccess().getIntervenantsPersonneCrossReference_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_17);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,23,FOLLOW_27); 

                    				newLeafNode(otherlv_10, grammarAccess.getUEAccess().getRightParenthesisKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:867:3: (otherlv_11= 'responsables' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==21) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalPortail.g:868:4: otherlv_11= 'responsables' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')'
                    {
                    otherlv_11=(Token)match(input,21,FOLLOW_16); 

                    				newLeafNode(otherlv_11, grammarAccess.getUEAccess().getResponsablesKeyword_6_0());
                    			
                    otherlv_12=(Token)match(input,22,FOLLOW_3); 

                    				newLeafNode(otherlv_12, grammarAccess.getUEAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalPortail.g:876:4: ( ( ruleEString ) )
                    // InternalPortail.g:877:5: ( ruleEString )
                    {
                    // InternalPortail.g:877:5: ( ruleEString )
                    // InternalPortail.g:878:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getUERule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getUEAccess().getResponsablesPersonneCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_17);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:892:4: (otherlv_14= ',' ( ( ruleEString ) ) )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==16) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // InternalPortail.g:893:5: otherlv_14= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_14=(Token)match(input,16,FOLLOW_3); 

                    	    					newLeafNode(otherlv_14, grammarAccess.getUEAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalPortail.g:897:5: ( ( ruleEString ) )
                    	    // InternalPortail.g:898:6: ( ruleEString )
                    	    {
                    	    // InternalPortail.g:898:6: ( ruleEString )
                    	    // InternalPortail.g:899:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getUERule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getUEAccess().getResponsablesPersonneCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_17);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,23,FOLLOW_28); 

                    				newLeafNode(otherlv_16, grammarAccess.getUEAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:919:3: (otherlv_17= 'contenus' otherlv_18= '{' ( (lv_contenus_19_0= ruleContenu ) ) (otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) ) )* otherlv_22= '}' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==25) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalPortail.g:920:4: otherlv_17= 'contenus' otherlv_18= '{' ( (lv_contenus_19_0= ruleContenu ) ) (otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) ) )* otherlv_22= '}'
                    {
                    otherlv_17=(Token)match(input,25,FOLLOW_4); 

                    				newLeafNode(otherlv_17, grammarAccess.getUEAccess().getContenusKeyword_7_0());
                    			
                    otherlv_18=(Token)match(input,12,FOLLOW_21); 

                    				newLeafNode(otherlv_18, grammarAccess.getUEAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalPortail.g:928:4: ( (lv_contenus_19_0= ruleContenu ) )
                    // InternalPortail.g:929:5: (lv_contenus_19_0= ruleContenu )
                    {
                    // InternalPortail.g:929:5: (lv_contenus_19_0= ruleContenu )
                    // InternalPortail.g:930:6: lv_contenus_19_0= ruleContenu
                    {

                    						newCompositeNode(grammarAccess.getUEAccess().getContenusContenuParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_contenus_19_0=ruleContenu();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUERule());
                    						}
                    						add(
                    							current,
                    							"contenus",
                    							lv_contenus_19_0,
                    							"org.xtext.fil.Portail.Contenu");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:947:4: (otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) ) )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==16) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // InternalPortail.g:948:5: otherlv_20= ',' ( (lv_contenus_21_0= ruleContenu ) )
                    	    {
                    	    otherlv_20=(Token)match(input,16,FOLLOW_21); 

                    	    					newLeafNode(otherlv_20, grammarAccess.getUEAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalPortail.g:952:5: ( (lv_contenus_21_0= ruleContenu ) )
                    	    // InternalPortail.g:953:6: (lv_contenus_21_0= ruleContenu )
                    	    {
                    	    // InternalPortail.g:953:6: (lv_contenus_21_0= ruleContenu )
                    	    // InternalPortail.g:954:7: lv_contenus_21_0= ruleContenu
                    	    {

                    	    							newCompositeNode(grammarAccess.getUEAccess().getContenusContenuParserRuleCall_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_contenus_21_0=ruleContenu();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getUERule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"contenus",
                    	    								lv_contenus_21_0,
                    	    								"org.xtext.fil.Portail.Contenu");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    otherlv_22=(Token)match(input,17,FOLLOW_14); 

                    				newLeafNode(otherlv_22, grammarAccess.getUEAccess().getRightCurlyBracketKeyword_7_4());
                    			

                    }
                    break;

            }

            otherlv_23=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_23, grammarAccess.getUEAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUE"


    // $ANTLR start "entryRuleEString"
    // InternalPortail.g:985:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalPortail.g:985:47: (iv_ruleEString= ruleEString EOF )
            // InternalPortail.g:986:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalPortail.g:992:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalPortail.g:998:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalPortail.g:999:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalPortail.g:999:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==RULE_STRING) ) {
                alt26=1;
            }
            else if ( (LA26_0==RULE_ID) ) {
                alt26=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }
            switch (alt26) {
                case 1 :
                    // InternalPortail.g:1000:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPortail.g:1008:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleParcours"
    // InternalPortail.g:1019:1: entryRuleParcours returns [EObject current=null] : iv_ruleParcours= ruleParcours EOF ;
    public final EObject entryRuleParcours() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParcours = null;


        try {
            // InternalPortail.g:1019:49: (iv_ruleParcours= ruleParcours EOF )
            // InternalPortail.g:1020:2: iv_ruleParcours= ruleParcours EOF
            {
             newCompositeNode(grammarAccess.getParcoursRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParcours=ruleParcours();

            state._fsp--;

             current =iv_ruleParcours; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParcours"


    // $ANTLR start "ruleParcours"
    // InternalPortail.g:1026:1: ruleParcours returns [EObject current=null] : ( () otherlv_1= 'Parcours' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'contenus' otherlv_11= '{' ( (lv_contenus_12_0= ruleContenu ) ) (otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) ) )* otherlv_15= '}' )? (otherlv_16= 'semestres' otherlv_17= '{' ( (lv_semestres_18_0= ruleSemestre ) ) (otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) ) )* otherlv_21= '}' )? otherlv_22= '}' ) ;
    public final EObject ruleParcours() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_contenus_12_0 = null;

        EObject lv_contenus_14_0 = null;

        EObject lv_semestres_18_0 = null;

        EObject lv_semestres_20_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:1032:2: ( ( () otherlv_1= 'Parcours' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'contenus' otherlv_11= '{' ( (lv_contenus_12_0= ruleContenu ) ) (otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) ) )* otherlv_15= '}' )? (otherlv_16= 'semestres' otherlv_17= '{' ( (lv_semestres_18_0= ruleSemestre ) ) (otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) ) )* otherlv_21= '}' )? otherlv_22= '}' ) )
            // InternalPortail.g:1033:2: ( () otherlv_1= 'Parcours' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'contenus' otherlv_11= '{' ( (lv_contenus_12_0= ruleContenu ) ) (otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) ) )* otherlv_15= '}' )? (otherlv_16= 'semestres' otherlv_17= '{' ( (lv_semestres_18_0= ruleSemestre ) ) (otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) ) )* otherlv_21= '}' )? otherlv_22= '}' )
            {
            // InternalPortail.g:1033:2: ( () otherlv_1= 'Parcours' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'contenus' otherlv_11= '{' ( (lv_contenus_12_0= ruleContenu ) ) (otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) ) )* otherlv_15= '}' )? (otherlv_16= 'semestres' otherlv_17= '{' ( (lv_semestres_18_0= ruleSemestre ) ) (otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) ) )* otherlv_21= '}' )? otherlv_22= '}' )
            // InternalPortail.g:1034:3: () otherlv_1= 'Parcours' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'contenus' otherlv_11= '{' ( (lv_contenus_12_0= ruleContenu ) ) (otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) ) )* otherlv_15= '}' )? (otherlv_16= 'semestres' otherlv_17= '{' ( (lv_semestres_18_0= ruleSemestre ) ) (otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) ) )* otherlv_21= '}' )? otherlv_22= '}'
            {
            // InternalPortail.g:1034:3: ()
            // InternalPortail.g:1035:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getParcoursAccess().getParcoursAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getParcoursAccess().getParcoursKeyword_1());
            		
            // InternalPortail.g:1045:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPortail.g:1046:4: (lv_name_2_0= ruleEString )
            {
            // InternalPortail.g:1046:4: (lv_name_2_0= ruleEString )
            // InternalPortail.g:1047:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getParcoursAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParcoursRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.xtext.fil.Portail.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_29); 

            			newLeafNode(otherlv_3, grammarAccess.getParcoursAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPortail.g:1068:3: (otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==21) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalPortail.g:1069:4: otherlv_4= 'responsables' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,21,FOLLOW_16); 

                    				newLeafNode(otherlv_4, grammarAccess.getParcoursAccess().getResponsablesKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,22,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getParcoursAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalPortail.g:1077:4: ( ( ruleEString ) )
                    // InternalPortail.g:1078:5: ( ruleEString )
                    {
                    // InternalPortail.g:1078:5: ( ruleEString )
                    // InternalPortail.g:1079:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getParcoursRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getParcoursAccess().getResponsablesPersonneCrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_17);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:1093:4: (otherlv_7= ',' ( ( ruleEString ) ) )*
                    loop27:
                    do {
                        int alt27=2;
                        int LA27_0 = input.LA(1);

                        if ( (LA27_0==16) ) {
                            alt27=1;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // InternalPortail.g:1094:5: otherlv_7= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_7=(Token)match(input,16,FOLLOW_3); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getParcoursAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalPortail.g:1098:5: ( ( ruleEString ) )
                    	    // InternalPortail.g:1099:6: ( ruleEString )
                    	    {
                    	    // InternalPortail.g:1099:6: ( ruleEString )
                    	    // InternalPortail.g:1100:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getParcoursRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getParcoursAccess().getResponsablesPersonneCrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_17);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop27;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,23,FOLLOW_30); 

                    				newLeafNode(otherlv_9, grammarAccess.getParcoursAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:1120:3: (otherlv_10= 'contenus' otherlv_11= '{' ( (lv_contenus_12_0= ruleContenu ) ) (otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) ) )* otherlv_15= '}' )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==25) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalPortail.g:1121:4: otherlv_10= 'contenus' otherlv_11= '{' ( (lv_contenus_12_0= ruleContenu ) ) (otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) ) )* otherlv_15= '}'
                    {
                    otherlv_10=(Token)match(input,25,FOLLOW_4); 

                    				newLeafNode(otherlv_10, grammarAccess.getParcoursAccess().getContenusKeyword_5_0());
                    			
                    otherlv_11=(Token)match(input,12,FOLLOW_21); 

                    				newLeafNode(otherlv_11, grammarAccess.getParcoursAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalPortail.g:1129:4: ( (lv_contenus_12_0= ruleContenu ) )
                    // InternalPortail.g:1130:5: (lv_contenus_12_0= ruleContenu )
                    {
                    // InternalPortail.g:1130:5: (lv_contenus_12_0= ruleContenu )
                    // InternalPortail.g:1131:6: lv_contenus_12_0= ruleContenu
                    {

                    						newCompositeNode(grammarAccess.getParcoursAccess().getContenusContenuParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_contenus_12_0=ruleContenu();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getParcoursRule());
                    						}
                    						add(
                    							current,
                    							"contenus",
                    							lv_contenus_12_0,
                    							"org.xtext.fil.Portail.Contenu");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:1148:4: (otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) ) )*
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( (LA29_0==16) ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // InternalPortail.g:1149:5: otherlv_13= ',' ( (lv_contenus_14_0= ruleContenu ) )
                    	    {
                    	    otherlv_13=(Token)match(input,16,FOLLOW_21); 

                    	    					newLeafNode(otherlv_13, grammarAccess.getParcoursAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalPortail.g:1153:5: ( (lv_contenus_14_0= ruleContenu ) )
                    	    // InternalPortail.g:1154:6: (lv_contenus_14_0= ruleContenu )
                    	    {
                    	    // InternalPortail.g:1154:6: (lv_contenus_14_0= ruleContenu )
                    	    // InternalPortail.g:1155:7: lv_contenus_14_0= ruleContenu
                    	    {

                    	    							newCompositeNode(grammarAccess.getParcoursAccess().getContenusContenuParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_contenus_14_0=ruleContenu();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getParcoursRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"contenus",
                    	    								lv_contenus_14_0,
                    	    								"org.xtext.fil.Portail.Contenu");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop29;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,17,FOLLOW_31); 

                    				newLeafNode(otherlv_15, grammarAccess.getParcoursAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalPortail.g:1178:3: (otherlv_16= 'semestres' otherlv_17= '{' ( (lv_semestres_18_0= ruleSemestre ) ) (otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) ) )* otherlv_21= '}' )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==33) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalPortail.g:1179:4: otherlv_16= 'semestres' otherlv_17= '{' ( (lv_semestres_18_0= ruleSemestre ) ) (otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,33,FOLLOW_4); 

                    				newLeafNode(otherlv_16, grammarAccess.getParcoursAccess().getSemestresKeyword_6_0());
                    			
                    otherlv_17=(Token)match(input,12,FOLLOW_23); 

                    				newLeafNode(otherlv_17, grammarAccess.getParcoursAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalPortail.g:1187:4: ( (lv_semestres_18_0= ruleSemestre ) )
                    // InternalPortail.g:1188:5: (lv_semestres_18_0= ruleSemestre )
                    {
                    // InternalPortail.g:1188:5: (lv_semestres_18_0= ruleSemestre )
                    // InternalPortail.g:1189:6: lv_semestres_18_0= ruleSemestre
                    {

                    						newCompositeNode(grammarAccess.getParcoursAccess().getSemestresSemestreParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_semestres_18_0=ruleSemestre();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getParcoursRule());
                    						}
                    						add(
                    							current,
                    							"semestres",
                    							lv_semestres_18_0,
                    							"org.xtext.fil.Portail.Semestre");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:1206:4: (otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) ) )*
                    loop31:
                    do {
                        int alt31=2;
                        int LA31_0 = input.LA(1);

                        if ( (LA31_0==16) ) {
                            alt31=1;
                        }


                        switch (alt31) {
                    	case 1 :
                    	    // InternalPortail.g:1207:5: otherlv_19= ',' ( (lv_semestres_20_0= ruleSemestre ) )
                    	    {
                    	    otherlv_19=(Token)match(input,16,FOLLOW_23); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getParcoursAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalPortail.g:1211:5: ( (lv_semestres_20_0= ruleSemestre ) )
                    	    // InternalPortail.g:1212:6: (lv_semestres_20_0= ruleSemestre )
                    	    {
                    	    // InternalPortail.g:1212:6: (lv_semestres_20_0= ruleSemestre )
                    	    // InternalPortail.g:1213:7: lv_semestres_20_0= ruleSemestre
                    	    {

                    	    							newCompositeNode(grammarAccess.getParcoursAccess().getSemestresSemestreParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_semestres_20_0=ruleSemestre();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getParcoursRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"semestres",
                    	    								lv_semestres_20_0,
                    	    								"org.xtext.fil.Portail.Semestre");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop31;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,17,FOLLOW_14); 

                    				newLeafNode(otherlv_21, grammarAccess.getParcoursAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_22=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_22, grammarAccess.getParcoursAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParcours"


    // $ANTLR start "entryRuleSemestre"
    // InternalPortail.g:1244:1: entryRuleSemestre returns [EObject current=null] : iv_ruleSemestre= ruleSemestre EOF ;
    public final EObject entryRuleSemestre() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSemestre = null;


        try {
            // InternalPortail.g:1244:49: (iv_ruleSemestre= ruleSemestre EOF )
            // InternalPortail.g:1245:2: iv_ruleSemestre= ruleSemestre EOF
            {
             newCompositeNode(grammarAccess.getSemestreRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSemestre=ruleSemestre();

            state._fsp--;

             current =iv_ruleSemestre; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSemestre"


    // $ANTLR start "ruleSemestre"
    // InternalPortail.g:1251:1: ruleSemestre returns [EObject current=null] : ( () otherlv_1= 'Semestre' otherlv_2= '{' (otherlv_3= 'numero' ( (lv_numero_4_0= ruleEInt ) ) )? (otherlv_5= 'ues' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' ) ;
    public final EObject ruleSemestre() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_numero_4_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:1257:2: ( ( () otherlv_1= 'Semestre' otherlv_2= '{' (otherlv_3= 'numero' ( (lv_numero_4_0= ruleEInt ) ) )? (otherlv_5= 'ues' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' ) )
            // InternalPortail.g:1258:2: ( () otherlv_1= 'Semestre' otherlv_2= '{' (otherlv_3= 'numero' ( (lv_numero_4_0= ruleEInt ) ) )? (otherlv_5= 'ues' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' )
            {
            // InternalPortail.g:1258:2: ( () otherlv_1= 'Semestre' otherlv_2= '{' (otherlv_3= 'numero' ( (lv_numero_4_0= ruleEInt ) ) )? (otherlv_5= 'ues' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' )
            // InternalPortail.g:1259:3: () otherlv_1= 'Semestre' otherlv_2= '{' (otherlv_3= 'numero' ( (lv_numero_4_0= ruleEInt ) ) )? (otherlv_5= 'ues' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}'
            {
            // InternalPortail.g:1259:3: ()
            // InternalPortail.g:1260:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSemestreAccess().getSemestreAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,34,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getSemestreAccess().getSemestreKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_32); 

            			newLeafNode(otherlv_2, grammarAccess.getSemestreAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPortail.g:1274:3: (otherlv_3= 'numero' ( (lv_numero_4_0= ruleEInt ) ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==35) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalPortail.g:1275:4: otherlv_3= 'numero' ( (lv_numero_4_0= ruleEInt ) )
                    {
                    otherlv_3=(Token)match(input,35,FOLLOW_33); 

                    				newLeafNode(otherlv_3, grammarAccess.getSemestreAccess().getNumeroKeyword_3_0());
                    			
                    // InternalPortail.g:1279:4: ( (lv_numero_4_0= ruleEInt ) )
                    // InternalPortail.g:1280:5: (lv_numero_4_0= ruleEInt )
                    {
                    // InternalPortail.g:1280:5: (lv_numero_4_0= ruleEInt )
                    // InternalPortail.g:1281:6: lv_numero_4_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getSemestreAccess().getNumeroEIntParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_12);
                    lv_numero_4_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSemestreRule());
                    						}
                    						set(
                    							current,
                    							"numero",
                    							lv_numero_4_0,
                    							"org.xtext.fil.Portail.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPortail.g:1299:3: (otherlv_5= 'ues' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==19) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalPortail.g:1300:4: otherlv_5= 'ues' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')'
                    {
                    otherlv_5=(Token)match(input,19,FOLLOW_16); 

                    				newLeafNode(otherlv_5, grammarAccess.getSemestreAccess().getUesKeyword_4_0());
                    			
                    otherlv_6=(Token)match(input,22,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getSemestreAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalPortail.g:1308:4: ( ( ruleEString ) )
                    // InternalPortail.g:1309:5: ( ruleEString )
                    {
                    // InternalPortail.g:1309:5: ( ruleEString )
                    // InternalPortail.g:1310:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSemestreRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getSemestreAccess().getUesUECrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_17);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPortail.g:1324:4: (otherlv_8= ',' ( ( ruleEString ) ) )*
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==16) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // InternalPortail.g:1325:5: otherlv_8= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_8=(Token)match(input,16,FOLLOW_3); 

                    	    					newLeafNode(otherlv_8, grammarAccess.getSemestreAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalPortail.g:1329:5: ( ( ruleEString ) )
                    	    // InternalPortail.g:1330:6: ( ruleEString )
                    	    {
                    	    // InternalPortail.g:1330:6: ( ruleEString )
                    	    // InternalPortail.g:1331:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getSemestreRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getSemestreAccess().getUesUECrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_17);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,23,FOLLOW_14); 

                    				newLeafNode(otherlv_10, grammarAccess.getSemestreAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            otherlv_11=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getSemestreAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSemestre"


    // $ANTLR start "entryRuleDescription"
    // InternalPortail.g:1359:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // InternalPortail.g:1359:52: (iv_ruleDescription= ruleDescription EOF )
            // InternalPortail.g:1360:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalPortail.g:1366:1: ruleDescription returns [EObject current=null] : ( () otherlv_1= 'Description' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'texte' ( (lv_texte_5_0= ruleEString ) ) )? otherlv_6= '}' ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_texte_5_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:1372:2: ( ( () otherlv_1= 'Description' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'texte' ( (lv_texte_5_0= ruleEString ) ) )? otherlv_6= '}' ) )
            // InternalPortail.g:1373:2: ( () otherlv_1= 'Description' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'texte' ( (lv_texte_5_0= ruleEString ) ) )? otherlv_6= '}' )
            {
            // InternalPortail.g:1373:2: ( () otherlv_1= 'Description' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'texte' ( (lv_texte_5_0= ruleEString ) ) )? otherlv_6= '}' )
            // InternalPortail.g:1374:3: () otherlv_1= 'Description' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'texte' ( (lv_texte_5_0= ruleEString ) ) )? otherlv_6= '}'
            {
            // InternalPortail.g:1374:3: ()
            // InternalPortail.g:1375:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDescriptionAccess().getDescriptionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,36,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getDescriptionAccess().getDescriptionKeyword_1());
            		
            // InternalPortail.g:1385:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPortail.g:1386:4: (lv_name_2_0= ruleEString )
            {
            // InternalPortail.g:1386:4: (lv_name_2_0= ruleEString )
            // InternalPortail.g:1387:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getDescriptionAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDescriptionRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.xtext.fil.Portail.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_34); 

            			newLeafNode(otherlv_3, grammarAccess.getDescriptionAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPortail.g:1408:3: (otherlv_4= 'texte' ( (lv_texte_5_0= ruleEString ) ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==37) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalPortail.g:1409:4: otherlv_4= 'texte' ( (lv_texte_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,37,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getDescriptionAccess().getTexteKeyword_4_0());
                    			
                    // InternalPortail.g:1413:4: ( (lv_texte_5_0= ruleEString ) )
                    // InternalPortail.g:1414:5: (lv_texte_5_0= ruleEString )
                    {
                    // InternalPortail.g:1414:5: (lv_texte_5_0= ruleEString )
                    // InternalPortail.g:1415:6: lv_texte_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getDescriptionAccess().getTexteEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_texte_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"texte",
                    							lv_texte_5_0,
                    							"org.xtext.fil.Portail.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getDescriptionAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleRessource"
    // InternalPortail.g:1441:1: entryRuleRessource returns [EObject current=null] : iv_ruleRessource= ruleRessource EOF ;
    public final EObject entryRuleRessource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRessource = null;


        try {
            // InternalPortail.g:1441:50: (iv_ruleRessource= ruleRessource EOF )
            // InternalPortail.g:1442:2: iv_ruleRessource= ruleRessource EOF
            {
             newCompositeNode(grammarAccess.getRessourceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRessource=ruleRessource();

            state._fsp--;

             current =iv_ruleRessource; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRessource"


    // $ANTLR start "ruleRessource"
    // InternalPortail.g:1448:1: ruleRessource returns [EObject current=null] : ( () otherlv_1= 'Ressource' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'url' ( (lv_url_5_0= ruleEString ) ) )? (otherlv_6= 'taille' ( (lv_taille_7_0= ruleEInt ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleRessource() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_url_5_0 = null;

        AntlrDatatypeRuleToken lv_taille_7_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:1454:2: ( ( () otherlv_1= 'Ressource' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'url' ( (lv_url_5_0= ruleEString ) ) )? (otherlv_6= 'taille' ( (lv_taille_7_0= ruleEInt ) ) )? otherlv_8= '}' ) )
            // InternalPortail.g:1455:2: ( () otherlv_1= 'Ressource' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'url' ( (lv_url_5_0= ruleEString ) ) )? (otherlv_6= 'taille' ( (lv_taille_7_0= ruleEInt ) ) )? otherlv_8= '}' )
            {
            // InternalPortail.g:1455:2: ( () otherlv_1= 'Ressource' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'url' ( (lv_url_5_0= ruleEString ) ) )? (otherlv_6= 'taille' ( (lv_taille_7_0= ruleEInt ) ) )? otherlv_8= '}' )
            // InternalPortail.g:1456:3: () otherlv_1= 'Ressource' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'url' ( (lv_url_5_0= ruleEString ) ) )? (otherlv_6= 'taille' ( (lv_taille_7_0= ruleEInt ) ) )? otherlv_8= '}'
            {
            // InternalPortail.g:1456:3: ()
            // InternalPortail.g:1457:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRessourceAccess().getRessourceAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,38,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getRessourceAccess().getRessourceKeyword_1());
            		
            // InternalPortail.g:1467:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPortail.g:1468:4: (lv_name_2_0= ruleEString )
            {
            // InternalPortail.g:1468:4: (lv_name_2_0= ruleEString )
            // InternalPortail.g:1469:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getRessourceAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRessourceRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.xtext.fil.Portail.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_35); 

            			newLeafNode(otherlv_3, grammarAccess.getRessourceAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPortail.g:1490:3: (otherlv_4= 'url' ( (lv_url_5_0= ruleEString ) ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==39) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalPortail.g:1491:4: otherlv_4= 'url' ( (lv_url_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,39,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getRessourceAccess().getUrlKeyword_4_0());
                    			
                    // InternalPortail.g:1495:4: ( (lv_url_5_0= ruleEString ) )
                    // InternalPortail.g:1496:5: (lv_url_5_0= ruleEString )
                    {
                    // InternalPortail.g:1496:5: (lv_url_5_0= ruleEString )
                    // InternalPortail.g:1497:6: lv_url_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getRessourceAccess().getUrlEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_36);
                    lv_url_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRessourceRule());
                    						}
                    						set(
                    							current,
                    							"url",
                    							lv_url_5_0,
                    							"org.xtext.fil.Portail.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPortail.g:1515:3: (otherlv_6= 'taille' ( (lv_taille_7_0= ruleEInt ) ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==40) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalPortail.g:1516:4: otherlv_6= 'taille' ( (lv_taille_7_0= ruleEInt ) )
                    {
                    otherlv_6=(Token)match(input,40,FOLLOW_33); 

                    				newLeafNode(otherlv_6, grammarAccess.getRessourceAccess().getTailleKeyword_5_0());
                    			
                    // InternalPortail.g:1520:4: ( (lv_taille_7_0= ruleEInt ) )
                    // InternalPortail.g:1521:5: (lv_taille_7_0= ruleEInt )
                    {
                    // InternalPortail.g:1521:5: (lv_taille_7_0= ruleEInt )
                    // InternalPortail.g:1522:6: lv_taille_7_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getRessourceAccess().getTailleEIntParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_taille_7_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRessourceRule());
                    						}
                    						set(
                    							current,
                    							"taille",
                    							lv_taille_7_0,
                    							"org.xtext.fil.Portail.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getRessourceAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRessource"


    // $ANTLR start "entryRuleSemainier"
    // InternalPortail.g:1548:1: entryRuleSemainier returns [EObject current=null] : iv_ruleSemainier= ruleSemainier EOF ;
    public final EObject entryRuleSemainier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSemainier = null;


        try {
            // InternalPortail.g:1548:50: (iv_ruleSemainier= ruleSemainier EOF )
            // InternalPortail.g:1549:2: iv_ruleSemainier= ruleSemainier EOF
            {
             newCompositeNode(grammarAccess.getSemainierRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSemainier=ruleSemainier();

            state._fsp--;

             current =iv_ruleSemainier; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSemainier"


    // $ANTLR start "ruleSemainier"
    // InternalPortail.g:1555:1: ruleSemainier returns [EObject current=null] : ( () otherlv_1= 'Semainier' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleSemainier() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalPortail.g:1561:2: ( ( () otherlv_1= 'Semainier' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalPortail.g:1562:2: ( () otherlv_1= 'Semainier' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalPortail.g:1562:2: ( () otherlv_1= 'Semainier' ( (lv_name_2_0= ruleEString ) ) )
            // InternalPortail.g:1563:3: () otherlv_1= 'Semainier' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalPortail.g:1563:3: ()
            // InternalPortail.g:1564:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSemainierAccess().getSemainierAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,41,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getSemainierAccess().getSemainierKeyword_1());
            		
            // InternalPortail.g:1574:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPortail.g:1575:4: (lv_name_2_0= ruleEString )
            {
            // InternalPortail.g:1575:4: (lv_name_2_0= ruleEString )
            // InternalPortail.g:1576:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSemainierAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSemainierRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.xtext.fil.Portail.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSemainier"


    // $ANTLR start "entryRuleEInt"
    // InternalPortail.g:1597:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalPortail.g:1597:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalPortail.g:1598:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalPortail.g:1604:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalPortail.g:1610:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalPortail.g:1611:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalPortail.g:1611:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalPortail.g:1612:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalPortail.g:1612:3: (kw= '-' )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==42) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalPortail.g:1613:4: kw= '-'
                    {
                    kw=(Token)match(input,42,FOLLOW_37); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000000EE000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000000000EC000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000000000E8000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000000E0000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000060000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000007220000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000810000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000007020000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000006020000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000025000000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000004020000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000010020000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000082220000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000002220000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000002020000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000202220000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000202020000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000200020000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x00000008000A0000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000040000000040L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000002000020000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000018000020000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000010000020000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000000040L});

}