/*
 * generated by Xtext 2.25.0
 */
package org.xtext.fil.serializer;

import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.xtext.fil.fil.Description;
import org.xtext.fil.fil.Fil;
import org.xtext.fil.fil.FilPackage;
import org.xtext.fil.fil.Formation;
import org.xtext.fil.fil.Parcours;
import org.xtext.fil.fil.Personne;
import org.xtext.fil.fil.Ressource;
import org.xtext.fil.fil.Semainier;
import org.xtext.fil.fil.Semestre;
import org.xtext.fil.fil.UE;
import org.xtext.fil.services.PortailGrammarAccess;

@SuppressWarnings("all")
public class PortailSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private PortailGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == FilPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case FilPackage.DESCRIPTION:
				sequence_Description(context, (Description) semanticObject); 
				return; 
			case FilPackage.FIL:
				sequence_Fil(context, (Fil) semanticObject); 
				return; 
			case FilPackage.FORMATION:
				sequence_Formation(context, (Formation) semanticObject); 
				return; 
			case FilPackage.PARCOURS:
				sequence_Parcours(context, (Parcours) semanticObject); 
				return; 
			case FilPackage.PERSONNE:
				sequence_Personne(context, (Personne) semanticObject); 
				return; 
			case FilPackage.RESSOURCE:
				sequence_Ressource(context, (Ressource) semanticObject); 
				return; 
			case FilPackage.SEMAINIER:
				sequence_Semainier(context, (Semainier) semanticObject); 
				return; 
			case FilPackage.SEMESTRE:
				sequence_Semestre(context, (Semestre) semanticObject); 
				return; 
			case FilPackage.UE:
				sequence_UE(context, (UE) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     Contenu returns Description
	 *     Description returns Description
	 *
	 * Constraint:
	 *     (name=EString texte=EString?)
	 */
	protected void sequence_Description(ISerializationContext context, Description semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Fil returns Fil
	 *
	 * Constraint:
	 *     (
	 *         name=EString 
	 *         description=EString? 
	 *         responsable=[Personne|EString]? 
	 *         (formations+=Formation formations+=Formation*)? 
	 *         (personnes+=Personne personnes+=Personne*)? 
	 *         (ues+=UE ues+=UE*)?
	 *     )
	 */
	protected void sequence_Fil(ISerializationContext context, Fil semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Formation returns Formation
	 *
	 * Constraint:
	 *     (
	 *         name=EString 
	 *         (responsables+=[Personne|EString] responsables+=[Personne|EString]*)? 
	 *         (parcours+=Parcours parcours+=Parcours*)? 
	 *         (contenus+=Contenu contenus+=Contenu*)? 
	 *         (semestre+=Semestre semestre+=Semestre*)?
	 *     )
	 */
	protected void sequence_Formation(ISerializationContext context, Formation semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Parcours returns Parcours
	 *
	 * Constraint:
	 *     (
	 *         name=EString 
	 *         (responsables+=[Personne|EString] responsables+=[Personne|EString]*)? 
	 *         (contenus+=Contenu contenus+=Contenu*)? 
	 *         (semestres+=Semestre semestres+=Semestre*)?
	 *     )
	 */
	protected void sequence_Parcours(ISerializationContext context, Parcours semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Personne returns Personne
	 *
	 * Constraint:
	 *     (name=EString prenom=EString?)
	 */
	protected void sequence_Personne(ISerializationContext context, Personne semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Contenu returns Ressource
	 *     Ressource returns Ressource
	 *
	 * Constraint:
	 *     (name=EString url=EString? taille=EInt?)
	 */
	protected void sequence_Ressource(ISerializationContext context, Ressource semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Contenu returns Semainier
	 *     Semainier returns Semainier
	 *
	 * Constraint:
	 *     name=EString
	 */
	protected void sequence_Semainier(ISerializationContext context, Semainier semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, FilPackage.Literals.CONTENU__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, FilPackage.Literals.CONTENU__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getSemainierAccess().getNameEStringParserRuleCall_2_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Semestre returns Semestre
	 *
	 * Constraint:
	 *     (numero=EInt? (ues+=[UE|EString] ues+=[UE|EString]*)?)
	 */
	protected void sequence_Semestre(ISerializationContext context, Semestre semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     UE returns UE
	 *
	 * Constraint:
	 *     (
	 *         optionnelle?='optionnelle'? 
	 *         name=EString 
	 *         (intervenants+=[Personne|EString] intervenants+=[Personne|EString]*)? 
	 *         (responsables+=[Personne|EString] responsables+=[Personne|EString]*)? 
	 *         (contenus+=Contenu contenus+=Contenu*)?
	 *     )
	 */
	protected void sequence_UE(ISerializationContext context, UE semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
}
