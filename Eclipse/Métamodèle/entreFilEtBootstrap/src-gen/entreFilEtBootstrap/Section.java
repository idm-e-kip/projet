/**
 */
package entreFilEtBootstrap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link entreFilEtBootstrap.Section#getTitre <em>Titre</em>}</li>
 *   <li>{@link entreFilEtBootstrap.Section#getTextes <em>Textes</em>}</li>
 * </ul>
 *
 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getSection()
 * @model
 * @generated
 */
public interface Section extends EObject {
	/**
	 * Returns the value of the '<em><b>Titre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Titre</em>' attribute.
	 * @see #setTitre(String)
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getSection_Titre()
	 * @model
	 * @generated
	 */
	String getTitre();

	/**
	 * Sets the value of the '{@link entreFilEtBootstrap.Section#getTitre <em>Titre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Titre</em>' attribute.
	 * @see #getTitre()
	 * @generated
	 */
	void setTitre(String value);

	/**
	 * Returns the value of the '<em><b>Textes</b></em>' containment reference list.
	 * The list contents are of type {@link entreFilEtBootstrap.Texte}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Textes</em>' containment reference list.
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getSection_Textes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Texte> getTextes();

} // Section
