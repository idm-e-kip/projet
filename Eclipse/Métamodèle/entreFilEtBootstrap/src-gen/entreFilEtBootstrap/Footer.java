/**
 */
package entreFilEtBootstrap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Footer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link entreFilEtBootstrap.Footer#getUrlImage <em>Url Image</em>}</li>
 *   <li>{@link entreFilEtBootstrap.Footer#getTextes <em>Textes</em>}</li>
 * </ul>
 *
 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getFooter()
 * @model
 * @generated
 */
public interface Footer extends EObject {
	/**
	 * Returns the value of the '<em><b>Url Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url Image</em>' attribute.
	 * @see #setUrlImage(String)
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getFooter_UrlImage()
	 * @model
	 * @generated
	 */
	String getUrlImage();

	/**
	 * Sets the value of the '{@link entreFilEtBootstrap.Footer#getUrlImage <em>Url Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url Image</em>' attribute.
	 * @see #getUrlImage()
	 * @generated
	 */
	void setUrlImage(String value);

	/**
	 * Returns the value of the '<em><b>Textes</b></em>' containment reference list.
	 * The list contents are of type {@link entreFilEtBootstrap.Texte}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Textes</em>' containment reference list.
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getFooter_Textes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Texte> getTextes();

} // Footer
