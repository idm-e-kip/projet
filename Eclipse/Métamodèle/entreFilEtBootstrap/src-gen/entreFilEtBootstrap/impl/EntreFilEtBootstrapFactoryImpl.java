/**
 */
package entreFilEtBootstrap.impl;

import entreFilEtBootstrap.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EntreFilEtBootstrapFactoryImpl extends EFactoryImpl implements EntreFilEtBootstrapFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EntreFilEtBootstrapFactory init() {
		try {
			EntreFilEtBootstrapFactory theEntreFilEtBootstrapFactory = (EntreFilEtBootstrapFactory) EPackage.Registry.INSTANCE
					.getEFactory(EntreFilEtBootstrapPackage.eNS_URI);
			if (theEntreFilEtBootstrapFactory != null) {
				return theEntreFilEtBootstrapFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EntreFilEtBootstrapFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntreFilEtBootstrapFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case EntreFilEtBootstrapPackage.MENU:
			return createMenu();
		case EntreFilEtBootstrapPackage.SECTION:
			return createSection();
		case EntreFilEtBootstrapPackage.PAGE:
			return createPage();
		case EntreFilEtBootstrapPackage.SITE:
			return createSite();
		case EntreFilEtBootstrapPackage.HEADER:
			return createHeader();
		case EntreFilEtBootstrapPackage.ITEM:
			return createItem();
		case EntreFilEtBootstrapPackage.FOOTER:
			return createFooter();
		case EntreFilEtBootstrapPackage.TEXTE:
			return createTexte();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Menu createMenu() {
		MenuImpl menu = new MenuImpl();
		return menu;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Section createSection() {
		SectionImpl section = new SectionImpl();
		return section;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Page createPage() {
		PageImpl page = new PageImpl();
		return page;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Site createSite() {
		SiteImpl site = new SiteImpl();
		return site;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Header createHeader() {
		HeaderImpl header = new HeaderImpl();
		return header;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Item createItem() {
		ItemImpl item = new ItemImpl();
		return item;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Footer createFooter() {
		FooterImpl footer = new FooterImpl();
		return footer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Texte createTexte() {
		TexteImpl texte = new TexteImpl();
		return texte;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntreFilEtBootstrapPackage getEntreFilEtBootstrapPackage() {
		return (EntreFilEtBootstrapPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EntreFilEtBootstrapPackage getPackage() {
		return EntreFilEtBootstrapPackage.eINSTANCE;
	}

} //EntreFilEtBootstrapFactoryImpl
