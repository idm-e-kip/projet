/**
 */
package entreFilEtBootstrap.impl;

import entreFilEtBootstrap.EntreFilEtBootstrapPackage;
import entreFilEtBootstrap.Footer;
import entreFilEtBootstrap.Header;
import entreFilEtBootstrap.Menu;
import entreFilEtBootstrap.Page;
import entreFilEtBootstrap.Section;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Page</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link entreFilEtBootstrap.impl.PageImpl#getMenus <em>Menus</em>}</li>
 *   <li>{@link entreFilEtBootstrap.impl.PageImpl#getSections <em>Sections</em>}</li>
 *   <li>{@link entreFilEtBootstrap.impl.PageImpl#getHautdepage <em>Hautdepage</em>}</li>
 *   <li>{@link entreFilEtBootstrap.impl.PageImpl#getFooter <em>Footer</em>}</li>
 *   <li>{@link entreFilEtBootstrap.impl.PageImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PageImpl extends MinimalEObjectImpl.Container implements Page {
	/**
	 * The cached value of the '{@link #getMenus() <em>Menus</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMenus()
	 * @generated
	 * @ordered
	 */
	protected Menu menus;

	/**
	 * The cached value of the '{@link #getSections() <em>Sections</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSections()
	 * @generated
	 * @ordered
	 */
	protected EList<Section> sections;

	/**
	 * The cached value of the '{@link #getHautdepage() <em>Hautdepage</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHautdepage()
	 * @generated
	 * @ordered
	 */
	protected Header hautdepage;

	/**
	 * The cached value of the '{@link #getFooter() <em>Footer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFooter()
	 * @generated
	 * @ordered
	 */
	protected Footer footer;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EntreFilEtBootstrapPackage.Literals.PAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Menu getMenus() {
		return menus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMenus(Menu newMenus, NotificationChain msgs) {
		Menu oldMenus = menus;
		menus = newMenus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					EntreFilEtBootstrapPackage.PAGE__MENUS, oldMenus, newMenus);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMenus(Menu newMenus) {
		if (newMenus != menus) {
			NotificationChain msgs = null;
			if (menus != null)
				msgs = ((InternalEObject) menus).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - EntreFilEtBootstrapPackage.PAGE__MENUS, null, msgs);
			if (newMenus != null)
				msgs = ((InternalEObject) newMenus).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - EntreFilEtBootstrapPackage.PAGE__MENUS, null, msgs);
			msgs = basicSetMenus(newMenus, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntreFilEtBootstrapPackage.PAGE__MENUS, newMenus,
					newMenus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Section> getSections() {
		if (sections == null) {
			sections = new EObjectContainmentEList<Section>(Section.class, this,
					EntreFilEtBootstrapPackage.PAGE__SECTIONS);
		}
		return sections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Header getHautdepage() {
		return hautdepage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHautdepage(Header newHautdepage, NotificationChain msgs) {
		Header oldHautdepage = hautdepage;
		hautdepage = newHautdepage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE, oldHautdepage, newHautdepage);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHautdepage(Header newHautdepage) {
		if (newHautdepage != hautdepage) {
			NotificationChain msgs = null;
			if (hautdepage != null)
				msgs = ((InternalEObject) hautdepage).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE, null, msgs);
			if (newHautdepage != null)
				msgs = ((InternalEObject) newHautdepage).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE, null, msgs);
			msgs = basicSetHautdepage(newHautdepage, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE,
					newHautdepage, newHautdepage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Footer getFooter() {
		return footer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFooter(Footer newFooter, NotificationChain msgs) {
		Footer oldFooter = footer;
		footer = newFooter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					EntreFilEtBootstrapPackage.PAGE__FOOTER, oldFooter, newFooter);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFooter(Footer newFooter) {
		if (newFooter != footer) {
			NotificationChain msgs = null;
			if (footer != null)
				msgs = ((InternalEObject) footer).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - EntreFilEtBootstrapPackage.PAGE__FOOTER, null, msgs);
			if (newFooter != null)
				msgs = ((InternalEObject) newFooter).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - EntreFilEtBootstrapPackage.PAGE__FOOTER, null, msgs);
			msgs = basicSetFooter(newFooter, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntreFilEtBootstrapPackage.PAGE__FOOTER, newFooter,
					newFooter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntreFilEtBootstrapPackage.PAGE__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.PAGE__MENUS:
			return basicSetMenus(null, msgs);
		case EntreFilEtBootstrapPackage.PAGE__SECTIONS:
			return ((InternalEList<?>) getSections()).basicRemove(otherEnd, msgs);
		case EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE:
			return basicSetHautdepage(null, msgs);
		case EntreFilEtBootstrapPackage.PAGE__FOOTER:
			return basicSetFooter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.PAGE__MENUS:
			return getMenus();
		case EntreFilEtBootstrapPackage.PAGE__SECTIONS:
			return getSections();
		case EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE:
			return getHautdepage();
		case EntreFilEtBootstrapPackage.PAGE__FOOTER:
			return getFooter();
		case EntreFilEtBootstrapPackage.PAGE__NAME:
			return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.PAGE__MENUS:
			setMenus((Menu) newValue);
			return;
		case EntreFilEtBootstrapPackage.PAGE__SECTIONS:
			getSections().clear();
			getSections().addAll((Collection<? extends Section>) newValue);
			return;
		case EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE:
			setHautdepage((Header) newValue);
			return;
		case EntreFilEtBootstrapPackage.PAGE__FOOTER:
			setFooter((Footer) newValue);
			return;
		case EntreFilEtBootstrapPackage.PAGE__NAME:
			setName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.PAGE__MENUS:
			setMenus((Menu) null);
			return;
		case EntreFilEtBootstrapPackage.PAGE__SECTIONS:
			getSections().clear();
			return;
		case EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE:
			setHautdepage((Header) null);
			return;
		case EntreFilEtBootstrapPackage.PAGE__FOOTER:
			setFooter((Footer) null);
			return;
		case EntreFilEtBootstrapPackage.PAGE__NAME:
			setName(NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.PAGE__MENUS:
			return menus != null;
		case EntreFilEtBootstrapPackage.PAGE__SECTIONS:
			return sections != null && !sections.isEmpty();
		case EntreFilEtBootstrapPackage.PAGE__HAUTDEPAGE:
			return hautdepage != null;
		case EntreFilEtBootstrapPackage.PAGE__FOOTER:
			return footer != null;
		case EntreFilEtBootstrapPackage.PAGE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //PageImpl
