/**
 */
package entreFilEtBootstrap.impl;

import entreFilEtBootstrap.EntreFilEtBootstrapPackage;
import entreFilEtBootstrap.Footer;
import entreFilEtBootstrap.Texte;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Footer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link entreFilEtBootstrap.impl.FooterImpl#getUrlImage <em>Url Image</em>}</li>
 *   <li>{@link entreFilEtBootstrap.impl.FooterImpl#getTextes <em>Textes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FooterImpl extends MinimalEObjectImpl.Container implements Footer {
	/**
	 * The default value of the '{@link #getUrlImage() <em>Url Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrlImage()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_IMAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUrlImage() <em>Url Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrlImage()
	 * @generated
	 * @ordered
	 */
	protected String urlImage = URL_IMAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTextes() <em>Textes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTextes()
	 * @generated
	 * @ordered
	 */
	protected EList<Texte> textes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FooterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EntreFilEtBootstrapPackage.Literals.FOOTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUrlImage() {
		return urlImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUrlImage(String newUrlImage) {
		String oldUrlImage = urlImage;
		urlImage = newUrlImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntreFilEtBootstrapPackage.FOOTER__URL_IMAGE,
					oldUrlImage, urlImage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Texte> getTextes() {
		if (textes == null) {
			textes = new EObjectContainmentEList<Texte>(Texte.class, this, EntreFilEtBootstrapPackage.FOOTER__TEXTES);
		}
		return textes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.FOOTER__TEXTES:
			return ((InternalEList<?>) getTextes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.FOOTER__URL_IMAGE:
			return getUrlImage();
		case EntreFilEtBootstrapPackage.FOOTER__TEXTES:
			return getTextes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.FOOTER__URL_IMAGE:
			setUrlImage((String) newValue);
			return;
		case EntreFilEtBootstrapPackage.FOOTER__TEXTES:
			getTextes().clear();
			getTextes().addAll((Collection<? extends Texte>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.FOOTER__URL_IMAGE:
			setUrlImage(URL_IMAGE_EDEFAULT);
			return;
		case EntreFilEtBootstrapPackage.FOOTER__TEXTES:
			getTextes().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.FOOTER__URL_IMAGE:
			return URL_IMAGE_EDEFAULT == null ? urlImage != null : !URL_IMAGE_EDEFAULT.equals(urlImage);
		case EntreFilEtBootstrapPackage.FOOTER__TEXTES:
			return textes != null && !textes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (urlImage: ");
		result.append(urlImage);
		result.append(')');
		return result.toString();
	}

} //FooterImpl
