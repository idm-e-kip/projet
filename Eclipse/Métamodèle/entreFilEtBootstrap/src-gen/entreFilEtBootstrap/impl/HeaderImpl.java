/**
 */
package entreFilEtBootstrap.impl;

import entreFilEtBootstrap.EntreFilEtBootstrapPackage;
import entreFilEtBootstrap.Header;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Header</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link entreFilEtBootstrap.impl.HeaderImpl#getUrlImage <em>Url Image</em>}</li>
 *   <li>{@link entreFilEtBootstrap.impl.HeaderImpl#getTitre <em>Titre</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HeaderImpl extends MinimalEObjectImpl.Container implements Header {
	/**
	 * The default value of the '{@link #getUrlImage() <em>Url Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrlImage()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_IMAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUrlImage() <em>Url Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrlImage()
	 * @generated
	 * @ordered
	 */
	protected String urlImage = URL_IMAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTitre() <em>Titre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitre()
	 * @generated
	 * @ordered
	 */
	protected static final String TITRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitre() <em>Titre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitre()
	 * @generated
	 * @ordered
	 */
	protected String titre = TITRE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HeaderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EntreFilEtBootstrapPackage.Literals.HEADER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUrlImage() {
		return urlImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUrlImage(String newUrlImage) {
		String oldUrlImage = urlImage;
		urlImage = newUrlImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntreFilEtBootstrapPackage.HEADER__URL_IMAGE,
					oldUrlImage, urlImage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTitre(String newTitre) {
		String oldTitre = titre;
		titre = newTitre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntreFilEtBootstrapPackage.HEADER__TITRE, oldTitre,
					titre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.HEADER__URL_IMAGE:
			return getUrlImage();
		case EntreFilEtBootstrapPackage.HEADER__TITRE:
			return getTitre();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.HEADER__URL_IMAGE:
			setUrlImage((String) newValue);
			return;
		case EntreFilEtBootstrapPackage.HEADER__TITRE:
			setTitre((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.HEADER__URL_IMAGE:
			setUrlImage(URL_IMAGE_EDEFAULT);
			return;
		case EntreFilEtBootstrapPackage.HEADER__TITRE:
			setTitre(TITRE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EntreFilEtBootstrapPackage.HEADER__URL_IMAGE:
			return URL_IMAGE_EDEFAULT == null ? urlImage != null : !URL_IMAGE_EDEFAULT.equals(urlImage);
		case EntreFilEtBootstrapPackage.HEADER__TITRE:
			return TITRE_EDEFAULT == null ? titre != null : !TITRE_EDEFAULT.equals(titre);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (urlImage: ");
		result.append(urlImage);
		result.append(", titre: ");
		result.append(titre);
		result.append(')');
		return result.toString();
	}

} //HeaderImpl
