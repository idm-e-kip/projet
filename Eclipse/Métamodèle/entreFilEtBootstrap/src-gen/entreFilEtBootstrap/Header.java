/**
 */
package entreFilEtBootstrap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Header</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link entreFilEtBootstrap.Header#getUrlImage <em>Url Image</em>}</li>
 *   <li>{@link entreFilEtBootstrap.Header#getTitre <em>Titre</em>}</li>
 * </ul>
 *
 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getHeader()
 * @model
 * @generated
 */
public interface Header extends EObject {
	/**
	 * Returns the value of the '<em><b>Url Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url Image</em>' attribute.
	 * @see #setUrlImage(String)
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getHeader_UrlImage()
	 * @model
	 * @generated
	 */
	String getUrlImage();

	/**
	 * Sets the value of the '{@link entreFilEtBootstrap.Header#getUrlImage <em>Url Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url Image</em>' attribute.
	 * @see #getUrlImage()
	 * @generated
	 */
	void setUrlImage(String value);

	/**
	 * Returns the value of the '<em><b>Titre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Titre</em>' attribute.
	 * @see #setTitre(String)
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getHeader_Titre()
	 * @model
	 * @generated
	 */
	String getTitre();

	/**
	 * Sets the value of the '{@link entreFilEtBootstrap.Header#getTitre <em>Titre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Titre</em>' attribute.
	 * @see #getTitre()
	 * @generated
	 */
	void setTitre(String value);

} // Header
