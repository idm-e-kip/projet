/**
 */
package entreFilEtBootstrap;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Page</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link entreFilEtBootstrap.Page#getMenus <em>Menus</em>}</li>
 *   <li>{@link entreFilEtBootstrap.Page#getSections <em>Sections</em>}</li>
 *   <li>{@link entreFilEtBootstrap.Page#getHautdepage <em>Hautdepage</em>}</li>
 *   <li>{@link entreFilEtBootstrap.Page#getFooter <em>Footer</em>}</li>
 *   <li>{@link entreFilEtBootstrap.Page#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getPage()
 * @model
 * @generated
 */
public interface Page extends EObject {
	/**
	 * Returns the value of the '<em><b>Menus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Menus</em>' containment reference.
	 * @see #setMenus(Menu)
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getPage_Menus()
	 * @model containment="true"
	 * @generated
	 */
	Menu getMenus();

	/**
	 * Sets the value of the '{@link entreFilEtBootstrap.Page#getMenus <em>Menus</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Menus</em>' containment reference.
	 * @see #getMenus()
	 * @generated
	 */
	void setMenus(Menu value);

	/**
	 * Returns the value of the '<em><b>Sections</b></em>' containment reference list.
	 * The list contents are of type {@link entreFilEtBootstrap.Section}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sections</em>' containment reference list.
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getPage_Sections()
	 * @model containment="true"
	 * @generated
	 */
	EList<Section> getSections();

	/**
	 * Returns the value of the '<em><b>Hautdepage</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hautdepage</em>' containment reference.
	 * @see #setHautdepage(Header)
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getPage_Hautdepage()
	 * @model containment="true"
	 * @generated
	 */
	Header getHautdepage();

	/**
	 * Sets the value of the '{@link entreFilEtBootstrap.Page#getHautdepage <em>Hautdepage</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hautdepage</em>' containment reference.
	 * @see #getHautdepage()
	 * @generated
	 */
	void setHautdepage(Header value);

	/**
	 * Returns the value of the '<em><b>Footer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Footer</em>' containment reference.
	 * @see #setFooter(Footer)
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getPage_Footer()
	 * @model containment="true"
	 * @generated
	 */
	Footer getFooter();

	/**
	 * Sets the value of the '{@link entreFilEtBootstrap.Page#getFooter <em>Footer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Footer</em>' containment reference.
	 * @see #getFooter()
	 * @generated
	 */
	void setFooter(Footer value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see entreFilEtBootstrap.EntreFilEtBootstrapPackage#getPage_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link entreFilEtBootstrap.Page#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Page
