/**
 */
package bootstrapmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ul</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.Ul#getLis <em>Lis</em>}</li>
 * </ul>
 *
 * @see bootstrapmm.BootstrapmmPackage#getUl()
 * @model
 * @generated
 */
public interface Ul extends Element {
	/**
	 * Returns the value of the '<em><b>Lis</b></em>' containment reference list.
	 * The list contents are of type {@link bootstrapmm.Li}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lis</em>' containment reference list.
	 * @see bootstrapmm.BootstrapmmPackage#getUl_Lis()
	 * @model containment="true"
	 * @generated
	 */
	EList<Li> getLis();

} // Ul
