/**
 */
package bootstrapmm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Head</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.Head#getHelements <em>Helements</em>}</li>
 * </ul>
 *
 * @see bootstrapmm.BootstrapmmPackage#getHead()
 * @model
 * @generated
 */
public interface Head extends EObject {
	/**
	 * Returns the value of the '<em><b>Helements</b></em>' containment reference list.
	 * The list contents are of type {@link bootstrapmm.HElement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Helements</em>' containment reference list.
	 * @see bootstrapmm.BootstrapmmPackage#getHead_Helements()
	 * @model containment="true"
	 * @generated
	 */
	EList<HElement> getHelements();

} // Head
