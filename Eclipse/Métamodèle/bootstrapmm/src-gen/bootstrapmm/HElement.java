/**
 */
package bootstrapmm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HElement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see bootstrapmm.BootstrapmmPackage#getHElement()
 * @model abstract="true"
 * @generated
 */
public interface HElement extends EObject {
} // HElement
