/**
 */
package bootstrapmm.impl;

import bootstrapmm.BootstrapmmPackage;
import bootstrapmm.Link;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.impl.LinkImpl#getRefh <em>Refh</em>}</li>
 *   <li>{@link bootstrapmm.impl.LinkImpl#getRel <em>Rel</em>}</li>
 *   <li>{@link bootstrapmm.impl.LinkImpl#getIntegrity <em>Integrity</em>}</li>
 *   <li>{@link bootstrapmm.impl.LinkImpl#getCrossorigin <em>Crossorigin</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LinkImpl extends HElementImpl implements Link {
	/**
	 * The default value of the '{@link #getRefh() <em>Refh</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefh()
	 * @generated
	 * @ordered
	 */
	protected static final String REFH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRefh() <em>Refh</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefh()
	 * @generated
	 * @ordered
	 */
	protected String refh = REFH_EDEFAULT;

	/**
	 * The default value of the '{@link #getRel() <em>Rel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRel()
	 * @generated
	 * @ordered
	 */
	protected static final String REL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRel() <em>Rel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRel()
	 * @generated
	 * @ordered
	 */
	protected String rel = REL_EDEFAULT;

	/**
	 * The default value of the '{@link #getIntegrity() <em>Integrity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegrity()
	 * @generated
	 * @ordered
	 */
	protected static final String INTEGRITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIntegrity() <em>Integrity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegrity()
	 * @generated
	 * @ordered
	 */
	protected String integrity = INTEGRITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getCrossorigin() <em>Crossorigin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCrossorigin()
	 * @generated
	 * @ordered
	 */
	protected static final String CROSSORIGIN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCrossorigin() <em>Crossorigin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCrossorigin()
	 * @generated
	 * @ordered
	 */
	protected String crossorigin = CROSSORIGIN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BootstrapmmPackage.Literals.LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRefh() {
		return refh;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefh(String newRefh) {
		String oldRefh = refh;
		refh = newRefh;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BootstrapmmPackage.LINK__REFH, oldRefh, refh));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRel() {
		return rel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRel(String newRel) {
		String oldRel = rel;
		rel = newRel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BootstrapmmPackage.LINK__REL, oldRel, rel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIntegrity() {
		return integrity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegrity(String newIntegrity) {
		String oldIntegrity = integrity;
		integrity = newIntegrity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BootstrapmmPackage.LINK__INTEGRITY, oldIntegrity,
					integrity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCrossorigin() {
		return crossorigin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCrossorigin(String newCrossorigin) {
		String oldCrossorigin = crossorigin;
		crossorigin = newCrossorigin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BootstrapmmPackage.LINK__CROSSORIGIN, oldCrossorigin,
					crossorigin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case BootstrapmmPackage.LINK__REFH:
			return getRefh();
		case BootstrapmmPackage.LINK__REL:
			return getRel();
		case BootstrapmmPackage.LINK__INTEGRITY:
			return getIntegrity();
		case BootstrapmmPackage.LINK__CROSSORIGIN:
			return getCrossorigin();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case BootstrapmmPackage.LINK__REFH:
			setRefh((String) newValue);
			return;
		case BootstrapmmPackage.LINK__REL:
			setRel((String) newValue);
			return;
		case BootstrapmmPackage.LINK__INTEGRITY:
			setIntegrity((String) newValue);
			return;
		case BootstrapmmPackage.LINK__CROSSORIGIN:
			setCrossorigin((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case BootstrapmmPackage.LINK__REFH:
			setRefh(REFH_EDEFAULT);
			return;
		case BootstrapmmPackage.LINK__REL:
			setRel(REL_EDEFAULT);
			return;
		case BootstrapmmPackage.LINK__INTEGRITY:
			setIntegrity(INTEGRITY_EDEFAULT);
			return;
		case BootstrapmmPackage.LINK__CROSSORIGIN:
			setCrossorigin(CROSSORIGIN_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case BootstrapmmPackage.LINK__REFH:
			return REFH_EDEFAULT == null ? refh != null : !REFH_EDEFAULT.equals(refh);
		case BootstrapmmPackage.LINK__REL:
			return REL_EDEFAULT == null ? rel != null : !REL_EDEFAULT.equals(rel);
		case BootstrapmmPackage.LINK__INTEGRITY:
			return INTEGRITY_EDEFAULT == null ? integrity != null : !INTEGRITY_EDEFAULT.equals(integrity);
		case BootstrapmmPackage.LINK__CROSSORIGIN:
			return CROSSORIGIN_EDEFAULT == null ? crossorigin != null : !CROSSORIGIN_EDEFAULT.equals(crossorigin);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (refh: ");
		result.append(refh);
		result.append(", rel: ");
		result.append(rel);
		result.append(", integrity: ");
		result.append(integrity);
		result.append(", crossorigin: ");
		result.append(crossorigin);
		result.append(')');
		return result.toString();
	}

} //LinkImpl
