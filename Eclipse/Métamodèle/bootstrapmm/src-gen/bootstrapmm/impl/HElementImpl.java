/**
 */
package bootstrapmm.impl;

import bootstrapmm.BootstrapmmPackage;
import bootstrapmm.HElement;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HElement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class HElementImpl extends MinimalEObjectImpl.Container implements HElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BootstrapmmPackage.Literals.HELEMENT;
	}

} //HElementImpl
