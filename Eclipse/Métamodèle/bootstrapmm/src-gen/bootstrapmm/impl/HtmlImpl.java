/**
 */
package bootstrapmm.impl;

import bootstrapmm.BootstrapmmPackage;
import bootstrapmm.Head;
import bootstrapmm.Html;

import bootstrapmm.SBody;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Html</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.impl.HtmlImpl#getHead <em>Head</em>}</li>
 *   <li>{@link bootstrapmm.impl.HtmlImpl#getSbody <em>Sbody</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HtmlImpl extends MinimalEObjectImpl.Container implements Html {
	/**
	 * The cached value of the '{@link #getHead() <em>Head</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHead()
	 * @generated
	 * @ordered
	 */
	protected Head head;

	/**
	 * The cached value of the '{@link #getSbody() <em>Sbody</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSbody()
	 * @generated
	 * @ordered
	 */
	protected SBody sbody;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HtmlImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BootstrapmmPackage.Literals.HTML;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Head getHead() {
		return head;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHead(Head newHead, NotificationChain msgs) {
		Head oldHead = head;
		head = newHead;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					BootstrapmmPackage.HTML__HEAD, oldHead, newHead);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHead(Head newHead) {
		if (newHead != head) {
			NotificationChain msgs = null;
			if (head != null)
				msgs = ((InternalEObject) head).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - BootstrapmmPackage.HTML__HEAD, null, msgs);
			if (newHead != null)
				msgs = ((InternalEObject) newHead).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - BootstrapmmPackage.HTML__HEAD, null, msgs);
			msgs = basicSetHead(newHead, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BootstrapmmPackage.HTML__HEAD, newHead, newHead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SBody getSbody() {
		return sbody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSbody(SBody newSbody, NotificationChain msgs) {
		SBody oldSbody = sbody;
		sbody = newSbody;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					BootstrapmmPackage.HTML__SBODY, oldSbody, newSbody);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSbody(SBody newSbody) {
		if (newSbody != sbody) {
			NotificationChain msgs = null;
			if (sbody != null)
				msgs = ((InternalEObject) sbody).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - BootstrapmmPackage.HTML__SBODY, null, msgs);
			if (newSbody != null)
				msgs = ((InternalEObject) newSbody).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - BootstrapmmPackage.HTML__SBODY, null, msgs);
			msgs = basicSetSbody(newSbody, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BootstrapmmPackage.HTML__SBODY, newSbody, newSbody));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case BootstrapmmPackage.HTML__HEAD:
			return basicSetHead(null, msgs);
		case BootstrapmmPackage.HTML__SBODY:
			return basicSetSbody(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case BootstrapmmPackage.HTML__HEAD:
			return getHead();
		case BootstrapmmPackage.HTML__SBODY:
			return getSbody();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case BootstrapmmPackage.HTML__HEAD:
			setHead((Head) newValue);
			return;
		case BootstrapmmPackage.HTML__SBODY:
			setSbody((SBody) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case BootstrapmmPackage.HTML__HEAD:
			setHead((Head) null);
			return;
		case BootstrapmmPackage.HTML__SBODY:
			setSbody((SBody) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case BootstrapmmPackage.HTML__HEAD:
			return head != null;
		case BootstrapmmPackage.HTML__SBODY:
			return sbody != null;
		}
		return super.eIsSet(featureID);
	}

} //HtmlImpl
