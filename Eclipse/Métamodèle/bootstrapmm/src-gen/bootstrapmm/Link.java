/**
 */
package bootstrapmm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.Link#getRefh <em>Refh</em>}</li>
 *   <li>{@link bootstrapmm.Link#getRel <em>Rel</em>}</li>
 *   <li>{@link bootstrapmm.Link#getIntegrity <em>Integrity</em>}</li>
 *   <li>{@link bootstrapmm.Link#getCrossorigin <em>Crossorigin</em>}</li>
 * </ul>
 *
 * @see bootstrapmm.BootstrapmmPackage#getLink()
 * @model
 * @generated
 */
public interface Link extends HElement {
	/**
	 * Returns the value of the '<em><b>Refh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refh</em>' attribute.
	 * @see #setRefh(String)
	 * @see bootstrapmm.BootstrapmmPackage#getLink_Refh()
	 * @model
	 * @generated
	 */
	String getRefh();

	/**
	 * Sets the value of the '{@link bootstrapmm.Link#getRefh <em>Refh</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refh</em>' attribute.
	 * @see #getRefh()
	 * @generated
	 */
	void setRefh(String value);

	/**
	 * Returns the value of the '<em><b>Rel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rel</em>' attribute.
	 * @see #setRel(String)
	 * @see bootstrapmm.BootstrapmmPackage#getLink_Rel()
	 * @model
	 * @generated
	 */
	String getRel();

	/**
	 * Sets the value of the '{@link bootstrapmm.Link#getRel <em>Rel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rel</em>' attribute.
	 * @see #getRel()
	 * @generated
	 */
	void setRel(String value);

	/**
	 * Returns the value of the '<em><b>Integrity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integrity</em>' attribute.
	 * @see #setIntegrity(String)
	 * @see bootstrapmm.BootstrapmmPackage#getLink_Integrity()
	 * @model
	 * @generated
	 */
	String getIntegrity();

	/**
	 * Sets the value of the '{@link bootstrapmm.Link#getIntegrity <em>Integrity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integrity</em>' attribute.
	 * @see #getIntegrity()
	 * @generated
	 */
	void setIntegrity(String value);

	/**
	 * Returns the value of the '<em><b>Crossorigin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Crossorigin</em>' attribute.
	 * @see #setCrossorigin(String)
	 * @see bootstrapmm.BootstrapmmPackage#getLink_Crossorigin()
	 * @model
	 * @generated
	 */
	String getCrossorigin();

	/**
	 * Sets the value of the '{@link bootstrapmm.Link#getCrossorigin <em>Crossorigin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Crossorigin</em>' attribute.
	 * @see #getCrossorigin()
	 * @generated
	 */
	void setCrossorigin(String value);

} // Link
