/**
 */
package bootstrapmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nav</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.Nav#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see bootstrapmm.BootstrapmmPackage#getNav()
 * @model
 * @generated
 */
public interface Nav extends Element {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link bootstrapmm.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see bootstrapmm.BootstrapmmPackage#getNav_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<Element> getElements();

} // Nav
