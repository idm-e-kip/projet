/**
 */
package bootstrapmm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BBody</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.BBody#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see bootstrapmm.BootstrapmmPackage#getBBody()
 * @model
 * @generated
 */
public interface BBody extends EObject {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link bootstrapmm.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see bootstrapmm.BootstrapmmPackage#getBBody_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<Element> getElements();

} // BBody
