/**
 */
package bootstrapmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>A</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.A#getRefh <em>Refh</em>}</li>
 *   <li>{@link bootstrapmm.A#getText <em>Text</em>}</li>
 *   <li>{@link bootstrapmm.A#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see bootstrapmm.BootstrapmmPackage#getA()
 * @model
 * @generated
 */
public interface A extends Element {
	/**
	 * Returns the value of the '<em><b>Refh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refh</em>' attribute.
	 * @see #setRefh(String)
	 * @see bootstrapmm.BootstrapmmPackage#getA_Refh()
	 * @model
	 * @generated
	 */
	String getRefh();

	/**
	 * Sets the value of the '{@link bootstrapmm.A#getRefh <em>Refh</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refh</em>' attribute.
	 * @see #getRefh()
	 * @generated
	 */
	void setRefh(String value);

	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see bootstrapmm.BootstrapmmPackage#getA_Text()
	 * @model
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link bootstrapmm.A#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link bootstrapmm.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see bootstrapmm.BootstrapmmPackage#getA_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<Element> getElements();

} // A
