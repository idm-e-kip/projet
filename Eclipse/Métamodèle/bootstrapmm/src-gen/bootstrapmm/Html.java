/**
 */
package bootstrapmm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Html</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.Html#getHead <em>Head</em>}</li>
 *   <li>{@link bootstrapmm.Html#getSbody <em>Sbody</em>}</li>
 * </ul>
 *
 * @see bootstrapmm.BootstrapmmPackage#getHtml()
 * @model
 * @generated
 */
public interface Html extends EObject {
	/**
	 * Returns the value of the '<em><b>Head</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Head</em>' containment reference.
	 * @see #setHead(Head)
	 * @see bootstrapmm.BootstrapmmPackage#getHtml_Head()
	 * @model containment="true"
	 * @generated
	 */
	Head getHead();

	/**
	 * Sets the value of the '{@link bootstrapmm.Html#getHead <em>Head</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Head</em>' containment reference.
	 * @see #getHead()
	 * @generated
	 */
	void setHead(Head value);

	/**
	 * Returns the value of the '<em><b>Sbody</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sbody</em>' containment reference.
	 * @see #setSbody(SBody)
	 * @see bootstrapmm.BootstrapmmPackage#getHtml_Sbody()
	 * @model containment="true"
	 * @generated
	 */
	SBody getSbody();

	/**
	 * Sets the value of the '{@link bootstrapmm.Html#getSbody <em>Sbody</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sbody</em>' containment reference.
	 * @see #getSbody()
	 * @generated
	 */
	void setSbody(SBody value);

} // Html
