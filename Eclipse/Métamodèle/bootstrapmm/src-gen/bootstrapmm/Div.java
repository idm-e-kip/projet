/**
 */
package bootstrapmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Div</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bootstrapmm.Div#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see bootstrapmm.BootstrapmmPackage#getDiv()
 * @model
 * @generated
 */
public interface Div extends Element {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link bootstrapmm.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see bootstrapmm.BootstrapmmPackage#getDiv_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<Element> getElements();

} // Div
