/**
 */
package fil;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fil.UE#getName <em>Name</em>}</li>
 *   <li>{@link fil.UE#isOptionnelle <em>Optionnelle</em>}</li>
 *   <li>{@link fil.UE#getIntervenants <em>Intervenants</em>}</li>
 *   <li>{@link fil.UE#getContenus <em>Contenus</em>}</li>
 *   <li>{@link fil.UE#getResponsables <em>Responsables</em>}</li>
 * </ul>
 *
 * @see fil.FilPackage#getUE()
 * @model
 * @generated
 */
public interface UE extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fil.FilPackage#getUE_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fil.UE#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Optionnelle</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optionnelle</em>' attribute.
	 * @see #setOptionnelle(boolean)
	 * @see fil.FilPackage#getUE_Optionnelle()
	 * @model default="false"
	 * @generated
	 */
	boolean isOptionnelle();

	/**
	 * Sets the value of the '{@link fil.UE#isOptionnelle <em>Optionnelle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optionnelle</em>' attribute.
	 * @see #isOptionnelle()
	 * @generated
	 */
	void setOptionnelle(boolean value);

	/**
	 * Returns the value of the '<em><b>Intervenants</b></em>' reference list.
	 * The list contents are of type {@link fil.Personne}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intervenants</em>' reference list.
	 * @see fil.FilPackage#getUE_Intervenants()
	 * @model
	 * @generated
	 */
	EList<Personne> getIntervenants();

	/**
	 * Returns the value of the '<em><b>Contenus</b></em>' containment reference list.
	 * The list contents are of type {@link fil.Contenu}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contenus</em>' containment reference list.
	 * @see fil.FilPackage#getUE_Contenus()
	 * @model containment="true"
	 * @generated
	 */
	EList<Contenu> getContenus();

	/**
	 * Returns the value of the '<em><b>Responsables</b></em>' reference list.
	 * The list contents are of type {@link fil.Personne}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsables</em>' reference list.
	 * @see fil.FilPackage#getUE_Responsables()
	 * @model
	 * @generated
	 */
	EList<Personne> getResponsables();

} // UE
