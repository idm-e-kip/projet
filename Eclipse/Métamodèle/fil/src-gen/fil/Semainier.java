/**
 */
package fil;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semainier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fil.FilPackage#getSemainier()
 * @model
 * @generated
 */
public interface Semainier extends Contenu {
} // Semainier
