/**
 */
package fil.impl;

import fil.FilPackage;
import fil.Semainier;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semainier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SemainierImpl extends ContenuImpl implements Semainier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemainierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilPackage.Literals.SEMAINIER;
	}

} //SemainierImpl
