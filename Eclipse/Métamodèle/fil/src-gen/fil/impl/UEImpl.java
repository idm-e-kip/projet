/**
 */
package fil.impl;

import fil.Contenu;
import fil.FilPackage;
import fil.Personne;
import fil.UE;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fil.impl.UEImpl#getName <em>Name</em>}</li>
 *   <li>{@link fil.impl.UEImpl#isOptionnelle <em>Optionnelle</em>}</li>
 *   <li>{@link fil.impl.UEImpl#getIntervenants <em>Intervenants</em>}</li>
 *   <li>{@link fil.impl.UEImpl#getContenus <em>Contenus</em>}</li>
 *   <li>{@link fil.impl.UEImpl#getResponsables <em>Responsables</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UEImpl extends MinimalEObjectImpl.Container implements UE {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isOptionnelle() <em>Optionnelle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptionnelle()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPTIONNELLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOptionnelle() <em>Optionnelle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptionnelle()
	 * @generated
	 * @ordered
	 */
	protected boolean optionnelle = OPTIONNELLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIntervenants() <em>Intervenants</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntervenants()
	 * @generated
	 * @ordered
	 */
	protected EList<Personne> intervenants;

	/**
	 * The cached value of the '{@link #getContenus() <em>Contenus</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContenus()
	 * @generated
	 * @ordered
	 */
	protected EList<Contenu> contenus;

	/**
	 * The cached value of the '{@link #getResponsables() <em>Responsables</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsables()
	 * @generated
	 * @ordered
	 */
	protected EList<Personne> responsables;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilPackage.Literals.UE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FilPackage.UE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOptionnelle() {
		return optionnelle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptionnelle(boolean newOptionnelle) {
		boolean oldOptionnelle = optionnelle;
		optionnelle = newOptionnelle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FilPackage.UE__OPTIONNELLE, oldOptionnelle,
					optionnelle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Personne> getIntervenants() {
		if (intervenants == null) {
			intervenants = new EObjectResolvingEList<Personne>(Personne.class, this, FilPackage.UE__INTERVENANTS);
		}
		return intervenants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Contenu> getContenus() {
		if (contenus == null) {
			contenus = new EObjectContainmentEList<Contenu>(Contenu.class, this, FilPackage.UE__CONTENUS);
		}
		return contenus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Personne> getResponsables() {
		if (responsables == null) {
			responsables = new EObjectResolvingEList<Personne>(Personne.class, this, FilPackage.UE__RESPONSABLES);
		}
		return responsables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FilPackage.UE__CONTENUS:
			return ((InternalEList<?>) getContenus()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FilPackage.UE__NAME:
			return getName();
		case FilPackage.UE__OPTIONNELLE:
			return isOptionnelle();
		case FilPackage.UE__INTERVENANTS:
			return getIntervenants();
		case FilPackage.UE__CONTENUS:
			return getContenus();
		case FilPackage.UE__RESPONSABLES:
			return getResponsables();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FilPackage.UE__NAME:
			setName((String) newValue);
			return;
		case FilPackage.UE__OPTIONNELLE:
			setOptionnelle((Boolean) newValue);
			return;
		case FilPackage.UE__INTERVENANTS:
			getIntervenants().clear();
			getIntervenants().addAll((Collection<? extends Personne>) newValue);
			return;
		case FilPackage.UE__CONTENUS:
			getContenus().clear();
			getContenus().addAll((Collection<? extends Contenu>) newValue);
			return;
		case FilPackage.UE__RESPONSABLES:
			getResponsables().clear();
			getResponsables().addAll((Collection<? extends Personne>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FilPackage.UE__NAME:
			setName(NAME_EDEFAULT);
			return;
		case FilPackage.UE__OPTIONNELLE:
			setOptionnelle(OPTIONNELLE_EDEFAULT);
			return;
		case FilPackage.UE__INTERVENANTS:
			getIntervenants().clear();
			return;
		case FilPackage.UE__CONTENUS:
			getContenus().clear();
			return;
		case FilPackage.UE__RESPONSABLES:
			getResponsables().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FilPackage.UE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case FilPackage.UE__OPTIONNELLE:
			return optionnelle != OPTIONNELLE_EDEFAULT;
		case FilPackage.UE__INTERVENANTS:
			return intervenants != null && !intervenants.isEmpty();
		case FilPackage.UE__CONTENUS:
			return contenus != null && !contenus.isEmpty();
		case FilPackage.UE__RESPONSABLES:
			return responsables != null && !responsables.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", optionnelle: ");
		result.append(optionnelle);
		result.append(')');
		return result.toString();
	}

} //UEImpl
