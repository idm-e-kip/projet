/**
 */
package fil;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parcours</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fil.Parcours#getName <em>Name</em>}</li>
 *   <li>{@link fil.Parcours#getResponsables <em>Responsables</em>}</li>
 *   <li>{@link fil.Parcours#getContenus <em>Contenus</em>}</li>
 *   <li>{@link fil.Parcours#getSemestres <em>Semestres</em>}</li>
 * </ul>
 *
 * @see fil.FilPackage#getParcours()
 * @model
 * @generated
 */
public interface Parcours extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fil.FilPackage#getParcours_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fil.Parcours#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Responsables</b></em>' reference list.
	 * The list contents are of type {@link fil.Personne}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsables</em>' reference list.
	 * @see fil.FilPackage#getParcours_Responsables()
	 * @model
	 * @generated
	 */
	EList<Personne> getResponsables();

	/**
	 * Returns the value of the '<em><b>Contenus</b></em>' containment reference list.
	 * The list contents are of type {@link fil.Contenu}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contenus</em>' containment reference list.
	 * @see fil.FilPackage#getParcours_Contenus()
	 * @model containment="true"
	 * @generated
	 */
	EList<Contenu> getContenus();

	/**
	 * Returns the value of the '<em><b>Semestres</b></em>' containment reference list.
	 * The list contents are of type {@link fil.Semestre}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semestres</em>' containment reference list.
	 * @see fil.FilPackage#getParcours_Semestres()
	 * @model containment="true"
	 * @generated
	 */
	EList<Semestre> getSemestres();

} // Parcours
