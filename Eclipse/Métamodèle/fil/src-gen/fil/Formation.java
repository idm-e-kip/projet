/**
 */
package fil;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fil.Formation#getName <em>Name</em>}</li>
 *   <li>{@link fil.Formation#getResponsables <em>Responsables</em>}</li>
 *   <li>{@link fil.Formation#getParcours <em>Parcours</em>}</li>
 *   <li>{@link fil.Formation#getContenus <em>Contenus</em>}</li>
 *   <li>{@link fil.Formation#getSemestre <em>Semestre</em>}</li>
 * </ul>
 *
 * @see fil.FilPackage#getFormation()
 * @model
 * @generated
 */
public interface Formation extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fil.FilPackage#getFormation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fil.Formation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Responsables</b></em>' reference list.
	 * The list contents are of type {@link fil.Personne}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsables</em>' reference list.
	 * @see fil.FilPackage#getFormation_Responsables()
	 * @model
	 * @generated
	 */
	EList<Personne> getResponsables();

	/**
	 * Returns the value of the '<em><b>Parcours</b></em>' containment reference list.
	 * The list contents are of type {@link fil.Parcours}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parcours</em>' containment reference list.
	 * @see fil.FilPackage#getFormation_Parcours()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parcours> getParcours();

	/**
	 * Returns the value of the '<em><b>Contenus</b></em>' containment reference list.
	 * The list contents are of type {@link fil.Contenu}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contenus</em>' containment reference list.
	 * @see fil.FilPackage#getFormation_Contenus()
	 * @model containment="true"
	 * @generated
	 */
	EList<Contenu> getContenus();

	/**
	 * Returns the value of the '<em><b>Semestre</b></em>' containment reference list.
	 * The list contents are of type {@link fil.Semestre}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semestre</em>' containment reference list.
	 * @see fil.FilPackage#getFormation_Semestre()
	 * @model containment="true"
	 * @generated
	 */
	EList<Semestre> getSemestre();

} // Formation
